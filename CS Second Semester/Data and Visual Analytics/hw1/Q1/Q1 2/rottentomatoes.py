"""
Rotten Tomatoes API:  http://developer.rottentomatoes.com/

Main file for interacting with the Rotten Tomatoes API.
"""

import urllib, gzip, StringIO, zlib

try:
    import json
except ImportError:  # pragma: no cover
    # For older versions of Python.
    import simplejson as json

try:
    from urllib2 import urlopen
except ImportError:  # pragma: no cover
    # For Python 3.
    from urllib.request import urlopen

from rottentomatoes_api_key import API_KEY


class RT(object):
    """
    An easy-to-use Python wrapper for the Rotten Tomatoes API.

    >>> RT('my-api-key').search('the lion king')

    Or, if your API key is stored in the `rottentomatoes_api_key.py` file,
    the RT class can be initialized like so:

    >>> RT().search('the lion king')
    """

    def __init__(self, api_key='', version = 1.0):
        if not api_key:
            self.api_key = API_KEY
        else:
            self.api_key = api_key

        if isinstance(version, int):
        	version = str(version)
        self.version = version
        
        BASE_URL = 'http://api.rottentomatoes.com/api/public/v%s/' % version
        self.BASE_URL = BASE_URL
        self.lists_url = BASE_URL + 'lists'
        self.movie_url = BASE_URL + 'movies'

    def _load_json_from_url(self, url):
    	"""
    	A wrapper around the api call. Returns a json-decoded object.
    	"""
    	response = urlopen(url).read()

        try:
            response = zlib.decompress(response, 16+zlib.MAX_WBITS)
        except zlib.error:
            pass

        return json.loads(response)
    	# if data[0:2] == "\x1f\x8b":
    	# 	data = gzip.GzipFile(fileobj = StringIO.StringIO(data)).read()
    	
    	# return json.loads(data)


    # def search(self, query, datatype = 'movies', page_limit = '30', page = '1'):
    def search(self, query, datatype = 'movies', page_limit = '50', page = '1'):
    	"""
    	Rotten Tomatoes movies search.
    	datatype: 'movies': the movies that are searched
    			  'total': the total count of search results
    	page_limit: The amount of movie search results to show per page
    	page: The selected page of movie search results
    	"""

        if isinstance(page_limit, int):
            page_limit = str(page_limit)
        
        if isinstance(page, int):
            page = str(page)

    	search_url = self.movie_url + '.json?apikey=' + self.api_key + '&q=' + query + '&page_limit=' + page_limit + '&page=' + page
    	data = self._load_json_from_url(search_url)

        return data[datatype]

    def movie_info(self, id, specific_info = None):
    	"""
    	Return infomation for a movie given its 'id'.
    	Arguments for 'specific_info' include 'cast' and 'clips'
    	"""
    	if isinstance(id, int):
    		id = str(id)

    	movie_url = self.movie_url + '/' + id 
    	if specific_info:
    		movie_url = movie_url + '/' + specific_info

    	movie_url = movie_url + '.json?apikey=' + self.api_key

    	data = self._load_json_from_url(movie_url)

    	return data

    def movie_similar(self, id, limit = '5'):
    	"""
    	Shows similar movies for a movie.
    	limit: Limit the number of similar movies to show
    	"""

    	if isinstance(id, int):
    		id = str(id)

    	if isinstance(limit, int):
    		limit = str(limit)

    	similar_url = self.movie_url + '/' + id + '/similar.json?apikey=' + self.api_key + '&limit' + limit

    	data = self._load_json_from_url(similar_url)

    	return data['movies']