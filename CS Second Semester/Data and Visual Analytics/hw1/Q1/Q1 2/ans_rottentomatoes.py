from rottentomatoes import RT
import time, csv

csvfile = file('movie_ID_name.csv', 'wb')
writer = csv.writer(csvfile)
writer.writerow(['movieID', 'moviename'])

fname = 'movie_ID_name.txt'
id_list = []

try:
	output = open(fname,'w')
except IOError:
	print '*** file open error:'
else:
	output.writelines('movieID,moviename\n')
	for i in range(6):
		data_star = RT().search('star', page = i + 1)
		time.sleep(0.2)
		for j in range(50):
			id_list.append(data_star[j]['id'])
			
			outlist = [data_star[j]['id'], ',', data_star[j]['title'], '\n']
			output.writelines(outlist)

			outcsv = [data_star[j]['id'], data_star[j]['title']]
			writer.writerow(outcsv)

	output.close()
	csvfile.close()

simMovieList = []

for i in range(300):
	similar_movie = RT().movie_similar(id = id_list[i])
	time.sleep(0.2)
	length = len(similar_movie)
	if length > 0:
		for j in range(length):
			pairMovie = [id_list[i], similar_movie[j]['id']]
			simMovieList.append(pairMovie)

lengthlist = len(simMovieList)
simMovie = []

for i in range(lengthlist):
	count = 0
	for j in range(len(simMovie)):
		if simMovie[j][0] == simMovieList[i][1] and simMovie[j][1] == simMovieList[i][0]:
			count = 1
			break

	if count == 0:
		simMovie.append(simMovieList[i])

fname = 'movie_ID_sim_movie_ID.txt'
csvname = 'movie_ID_sim_movie_ID.csv'

simfile = file(csvname, 'wb')
simwriter = csv.writer(simfile)
simwriter.writerow(['movieID', 'SimilarMovieID'])

try:
	outputsim = open(fname, 'w')
except IOError:
	print '*** file open error:'
else:
	outputsim.writelines('movieID,SimilarMovieID\n')
	for i in range(len(simMovie)):
		outlist = [simMovie[i][0], ',', simMovie[i][1], '\n']
		outputsim.writelines(outlist)
		
		outcsv = [simMovie[i][0], simMovie[i][1]]
		simwriter.writerow(outcsv)

	outputsim.close()
	simfile.close()