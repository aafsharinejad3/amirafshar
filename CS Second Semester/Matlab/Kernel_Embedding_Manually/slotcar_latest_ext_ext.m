


% 
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Initializations
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear all;
clc;
% 
% Path to svlab
addpath('\Users\lab\Desktop\Cs First Semester\Matlab\SVLab\svlab');

clipStart = 0;
clipEnd = 0;
trainingData = 1000;
hankelSize = 250;
simExtent = 5000;
lowD = 500;

% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Load Data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading Data');
pause(.1);
load -ASCII 110909_2
Y = X110909_2(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;

Ytrain = Y(1:floor(0.8*8403));
Ytest = Y(floor(0.8*8403)+1:end);
[d,M] = size(Ytrain);
load alpha_b_test.mat
alpha_c = alpha_b;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build Hankel Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Building Hankel matrix');
pause(.1);
tic
tau = M-hankelSize+1;    
X = zeros(hankelSize*d, tau);
for i = 1:hankelSize
       X((i-1)*d + 1:i*d,1:tau) = Ytrain(:,(1:tau)-1 + i);
end
M = tau;
toc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute HSE-PSR Paramters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Learning Model');
pause(.1);
tic
futures      = X(:, 1:M-2);  % Immediate future sequences of observations 
observations = X(1:d, 1:M-2);% Current observation
extfutures   = X(:,2:M);    % Future one-step-removed sequences of observations
extextfutures   = X(:,3:M);    % Future one-step-removed sequences of observations
%%
% Find kernel bandwidth as the median of the pairwise distances (the median
% trick).

% % Bandwidth for sequences of obs
disx = fastDist(X(:,1:M),X(:,1:M));
disx = disx(:);
mdisx = median(disx);

% Gaussian RBF Kernel (for observation sequences: histories & futures)
kernelx = rbf_dot(1./mdisx);

% Bandwidth for current obs
disx2 = fastDist(observations(:,1:M-2),observations(:,1:M-2));
disx2 = disx2(:);
mdisx2 = median(disx2);

% Gaussian RBF Kernel (for current observations)
kernelx2 = rbf_dot(1./mdisx2);

% Computing the Gram matrices.
%    
lambda = 0.1;
lambda_tilda = 0.1;
startFrame = 1000;
[B , K , K_x] = mean_kce(observations , lambda , Ytest , kernelx2);
% alpha = K(:,startFrame-1);

 alpha = alpha_c(:,60);
[w w_no_alpha] = mean_ksr( alpha , extextfutures , futures , lambda , kernelx);
%%
toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulating
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Simulating');
pause(.1);
tic


%%
tic
nextOb = [];
alpha_b = [];
for i = 1:120
i
     tic
    % Kernel Bayes' Rule
    alpha = mean_kbr(alpha,extextfutures,observations,futures,lambda,lambda,kernelx,kernelx2,Ytest(i),K,K_x(:,i),w_no_alpha);
    toc
    
    % Renormalize for numerical stability
    alpha = alpha./sum(alpha);
    
    %Square distribution, predict next observation as expectation wrt
    %squared distribution
%     Probs = (alpha).^2;
%     Probs = Probs./sum(Probs);
    nextOb = [nextOb,observations*alpha];
    alpha_b = [alpha_b,alpha];
%     
%     alpha = mean_kbr_2(alpha,extfutures,observations,futures,1e-1,1e-1,kernelx,kernelx2,nextOb);
%     G_Onew = sv_dot(kernelx2, observations, nextOb); 

    

end
toc
%%
% load('next_ob_irreg_3.mat');
% ob = nextOb;
% load('next_ob_irreg_4.mat');
% ob_2 = nextOb;
% % figure()
% % plot (linspace(1,30,30),Ytest(1:30),'g*',linspace(1,30,30),ob,'r*',linspace(1,30,30),ob_2,'k*');
 figure()
plot(linspace(1,47,47),nextOb,'*r',linspace(1,60,60),Ytest(1:60),'*g');
ylim([0 1])