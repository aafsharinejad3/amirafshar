%% In this m-file the difference with the file "windTurbine_latest_functional.m" is that we use extended extended futures (2 steps towards fututre) instead of just extended future.



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Initializations
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear all;
clc;
% 
% Path to svlab
addpath('../SVLab/svlab');

clipStart = 0;
clipEnd = 0;
trainingData = 2500;
hankelSize = 250;
simExtent = 5000;
lowD = 500;

% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Load Data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading Data');
pause(.1);
tic
load './windfarm_medium.mat';
Y = images(:,clipStart+1:end-clipEnd);
[Uy,Sy,Vy] = svd(Y(:,1:trainingData),'econ');

Uy = Uy(:,1:lowD);
Sy = Sy(1:lowD,1:lowD);
Vy = Vy(:,1:lowD);
Ytrain = Sy*Vy';
clear Y;
[d,M] = size(Ytrain);
toc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build Hankel Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Building Hankel matrix');
pause(.1);
tic
tau = M-hankelSize+1;    
X = zeros(hankelSize*d, tau);
for i = 1:hankelSize
       X((i-1)*d + 1:i*d,1:tau) = Ytrain(:,(1:tau)-1 + i);
end
M = tau;
toc

%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute HSE-PSR Paramters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Learning Model');
pause(.1);
tic
futures      = X(:, 1:M-2);  % Immediate future sequences of observations 
observations = X(1:d, 1:M-2);% Current observation
% extfutures   = X(:,2:M);    % Future one-step-removed sequences of observations
extextfutures = X(:, 3:M);
% Find kernel bandwidth as the median of the pairwise distances (the median
% trick).

% % Bandwidth for sequences of obs
disx = fastDist(X(:,1:M),X(:,1:M));
disx = disx(:);
mdisx = median(disx);

% Gaussian RBF Kernel (for observation sequences: histories & futures)
kernelx = rbf_dot(1./mdisx);

% Bandwidth for current obs
disx2 = fastDist(observations(:,1:M-2),observations(:,1:M-2));
disx2 = disx2(:);
mdisx2 = median(disx2);

% Gaussian RBF Kernel (for current observations)
kernelx2 = rbf_dot(1./mdisx2);

% Computing the Gram matrices.
%     
% G_TT  = sv_dot(kernelx, futures); % Kernel matrix for tests
% G_TTT = sv_dot(kernelx, futures, extfutures); % cross kernel
% G_OO  = sv_dot(kernelx2, observations); % kernel matrix for *single* obs
% sp1 = size(G_OO,1);
%  T = inv(G_TT + 1e-1*speye(sp1))*G_TTT;
%%
[T_alpha , T] = mean_ksr(ones(2249,1),extextfutures,futures,1e-1,kernelx);
toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulating
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Simulating');
pause(.1);
tic
startFrame = 1000;
% alpha  = zeros(size(T,2),1);
% alpha(startFrame-1) = 1;
K = sv_dot(kernelx,observations);
alpha = K(:,startFrame-1);
[w w_no_alpha] = mean_ksr( alpha , extextfutures , futures , 1e-1 , kernelx);

alpha_b = [];
%states = zeros(size(state,1),simExtent);
%  alpha = mean_kbr_2(alpha,extfutures,observations,futures,1e-1,1e-1,kernelx,kernelx2,999);
 alpha_b = [alpha_b,alpha];
% G_Onew = G_OO(:,startFrame);
toc
writerObj = VideoWriter('windsim.avi', 'Uncompressed AVI');
open(writerObj);
%%
tic
for i = 1:60
i
    % Kernel Bayes' Rule
    
   
%     state = T*alpha;
%     state = state./sum(state);
%     D = spdiags(state, 0, sp1, sp1);
%     G_new = D*G_OO;
%     alpha = (G_new*inv(G_new^2 + 1e-1*speye(sp1)))*...
%                     (D*G_Onew);

    
    % Renormalize for numerical stability
    alpha = alpha./sum(alpha);
    alpha_b = [alpha_b,alpha];
    %Square distribution, predict next observation as expectation wrt
    %squared distribution
    Probs = (alpha).^2;
    Probs = Probs./sum(Probs);
    nextOb = observations*Probs;
    
    [B , K , K_x] = mean_kce(observations , 1e-1 , nextOb , kernelx2);
    alpha = mean_kbr(alpha,extextfutures,observations,futures,1e-1,1e-1,kernelx,kernelx2,nextOb,K,K_x,w_no_alpha);
%     G_Onew = sv_dot(kernelx2, observations, nextOb);
    
    
    % Plot predicted image
    imagesc(reshape(uint8(Uy*nextOb), x, y, z));
    axis off
    axis equal
    truesize    
    pause(.01)
    if i > 1
        frame = getframe;
        writeVideo(writerObj,frame);
    end
    

    

end
toc
close(writerObj);

