
function [ a ] = mean_kbr( alpha , Y_p , X , Y , lambda ,lambda_tilda , kernel1 , kernel2 , x , K , K_x , w_no_alpha)
%UNTITLED12 Summary of this function goes here
%   Detailed explanation goes here
[n,m]=size(X);
gamma = w_no_alpha*spdiags(alpha,0,length(alpha),length(alpha));
% D = cov_ksr( alpha , Y_p , Y , lambda , kernel1);
% [B , K , K_x] = mean_kce(X , lambda , x , kernel2);
w = w_no_alpha*alpha;
D = spdiags(w,0,length(w),length(w));
I=speye(m);
DK = D*K;
int = inv((DK)^2+lambda_tilda*I);
a = gamma'*K*int*D*K_x;
end

