function [ B , K , K_x ] = mean_kce( X , lambda , x , kernel)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
[n,m] = size(X);
K = sv_dot(kernel,X);
[p,q] = size(x);
if (p ~= n)
    disp('The number of dimensions for "x" and "X" must be equal, please try again:')
    return
elseif (p == n)
    K_x = sv_dot(kernel,x,X)';
end

I = speye(m);
a = inv(K+lambda*I);
 B = a*K_x;

end

