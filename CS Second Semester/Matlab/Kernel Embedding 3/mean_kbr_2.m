function [ a ] = mean_kbr_2( alpha , Y_p , X , Y , lambda ,lambda_tilda , kernel , x)
%UNTITLED14 Summary of this function goes here
%   Detailed explanation goes here
[n,m]=size(X);
gamma = cov_kcr_2( alpha , Y_p , Y , lambda , kernel);
D = cov_kcr( alpha , Y_p , Y , lambda , kernel);
[B , K , K_x] = mean_kce(X , lambda , x , kernel);

I = eye(m);
int = inv((D*K)^2+lambda_tilda*I);
a = D*K*int*D*K_x;
end

