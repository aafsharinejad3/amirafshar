function [ B , K , K_x ] = mean_kce( X , lambda , x , kernel)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
[n,m] = size(X);
[p,q] = size(x);
if (p ~= n)
    disp('The number of dimensions for "x" and "X" must be equal, please try again:')
    return
end

K = sv_dot(kernel,X);
I = eye(m);
a = inv(K+lambda*I);

K_x = sv_dot(kernel,x,X)';
B = a*K_x;

end

