%%  Demo #1 Univariate Normal %%
% in order to generate a univariate random number, use any real numbers \mu
% and \sigma to define the distribution.
mu = 6.2; %the mean
sigma_squared = 2; %sigma^2 is the variance
%randn returns a random number from the standard normal distribution, which
%is a univariate normal with \mu = 0, \sigma = 1
standard_normal_random_number = randn;
%this is converted to an arbitrary univariate gaussian normal by:
normal_random_number = sqrt(sigma_squared) * standard_normal_random_number + mu;
%now, we can plot what's going on:
for i=1:100000
    standard_normal_random_number(i) = randn;
    normal_random_number(i) = sqrt(sigma_squared) * standard_normal_random_number(i) + mu;
end
hist([standard_normal_random_number' normal_random_number'],50)
legend('standard', 'custom')


%% Demo #2 Multivariate Normal %%
%The same trick works for the multivariate normal. \Mu has to be a vector,
%and \Sigma a symetric semidefinite matrix
Mu = [3 ;0]; %the mean
%a matrix is symetric iff M(a,b) = M(b,a), a positive semidefinite iff

% x*M*x'>=0 for any vector x. These properties are satisfied by all matrices
% which are taken from the matlab function gallery('randcorr',n)
Sigma =  [ 1.0000   -0.9195;  -0.9195    1.0000];
% to find A such that A'*A = Sigma, we find the eigendecompositon of Sigma:

% V'*D*V = Sigma
[V,D]=eig(Sigma);
A=V*(D.^(1/2));

%The standard multivariate normal is just a series of independently drawn
%univariate normal random numbers
standard_random_vector = randn(2,1);
%this is converted to an arbitrary multivariate normal by:
normal_random_vector = A * standard_random_vector + Mu;
 
%now, we can plot what's going on:
for i=1:1000
    standard_random_vector(:,i) = randn(2,1);
    normal_random_vector(:,i) = A * standard_random_vector(:,i) + Mu;
end
plot(standard_random_vector(1,:),standard_random_vector(2,:),'.')
hold on
plot(normal_random_vector(1,:),normal_random_vector(2,:),'r.')
legend('standard', 'custom')


%% Demo #3 Gaussian Process %%
% one way of looking at a Gaussian Process is as a Multivariate Normal with
% an infinite number of dimensions. However, in order to model
% relationships between points, we construct the covariance matrix with a

% function that defines the value of the matrix for any pair of real numbers:

sigma_f = 1.1251; %parameter of the squared exponential kernel

l =  0.90441; %parameter of the squared exponential kernel

kernel_function = @(x,x2) sigma_f^2*exp((x-x2)^2/(-2*l^2));

 
%This is one of many popular kernel functions, the squared exponential

%kernel. It favors smooth functions. (Here, it is defined here as an anonymous

%function handle)

% we can also define an error function, which models the observation noise
sigma_n = 0.1; %known noise on observed data
error_function = @(x,x2) sigma_n^2*(x==x2); 

%this is just iid gaussian noise with mean 0 and variance sigma_n^2s

%kernel functions can be added together. Here, we add the error kernel to
%the squared exponential kernel)
k = @(x,x2) kernel_function(x,x2)+error_function(x,x2);
%We can find the mean and the variance of the GP at each point
prediction_x=-2:0.01:1;
for i=1:length(prediction_x)
    mean(i) = 0;
    variance(i) = k(prediction_x(i),prediction_x(i));
end

plot_variance = @(x,lower,upper,color) set(fill([x,x(end:-1:1)],[upper,fliplr(lower)],color),'EdgeColor',color);
plot_variance(prediction_x,mean-1.96*variance,mean+1.96*variance,[0.9 0.9 0.9])
hold on
set(plot(prediction_x,mean,'k'),'LineWidth',2)

