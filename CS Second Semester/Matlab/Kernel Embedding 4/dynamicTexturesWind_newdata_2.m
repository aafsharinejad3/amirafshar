% 
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Initializations
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear all;
clc;
% 
addpath('\Users\lab\Desktop\CS First Semester\Matlab\SVLab\svlab');
% Path to svlab
clipStart = 0;
clipEnd = 0;
trainingData = 2500;
hankelSize = 250;
simExtent = 5000;
lowD = 500;

% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Load Data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading Data');
pause(.1);
load -ASCII 110909_2
Y = X110909_2(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;

Ytrain = Y(1:floor(0.8*8403));
Ytest = Y(floor(0.8*8403)+1:end);
[d,M] = size(Ytrain);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build Hankel Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Building Hankel matrix');
pause(.1);
tic
tau = M-hankelSize+1;    
X = zeros(hankelSize*d, tau);
for i = 1:hankelSize
       X((i-1)*d + 1:i*d,1:tau) = Ytrain(:,(1:tau)-1 + i);
end
M = tau;
toc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute HSE-PSR Paramters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Learning Model');
pause(.1);
tic
futures      = X(:, 1:M-1);  % Immediate future sequences of observations 
observations = X(1:d, 1:M-1);% Current observation
extfutures   = X(:,2:M);    % Future one-step-removed sequences of observations
%%
% Find kernel bandwidth as the median of the pairwise distances (the median
% trick).

% % Bandwidth for sequences of obs
disx = fastDist(X(:,1:M),X(:,1:M));
disx = disx(:);
mdisx = median(disx);

% Gaussian RBF Kernel (for observation sequences: histories & futures)
kernelx = rbf_dot(1./mdisx);

% Bandwidth for current obs
disx2 = fastDist(observations(:,1:M-1),observations(:,1:M-1));
disx2 = disx2(:);
mdisx2 = median(disx2);

% Gaussian RBF Kernel (for current observations)
kernelx2 = rbf_dot(1./mdisx2);

% Computing the Gram matrices.
%     
% G_TT  = sv_dot(kernelx, futures); % Kernel matrix for tests
% G_TTT = sv_dot(kernelx, futures, extfutures); % cross kernel
% G_OO  = sv_dot(kernelx2, observations); % kernel matrix for *single* obs
[B , K , K_x] = mean_kce(observations , 1e-1 , Ytest , kernelx2);
% sp1 = size(G_OO,1);
%  T = inv(G_TT + 1e-1*speye(sp1))*G_TTT;
%%
[T_alpha , T] = mean_ksr(ones(6472,1),extfutures,futures,1e-1,kernelx);
toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulating
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Simulating');
pause(.1);
tic
startFrame = 1000;
alpha  = zeros(size(T,2),1);
alpha(startFrame-1) = 1;

%%
%states = zeros(size(state,1),simExtent);
 
% G_Onew = G_OO(:,startFrame);
toc
% writerObj = VideoWriter('windsim.avi', 'Uncompressed AVI');
% open(writerObj);
%%
tic
nextOb = [];
for i = 1:100
i
    % Kernel Bayes' Rule
    alpha = mean_kbr(alpha,extfutures,observations,futures,1e-1,1e-1,kernelx,kernelx2,Ytest(i),K,K_x(:,i));
   
%     state = T*alpha;
%     state = state./sum(state);
%     D = spdiags(state, 0, sp1, sp1);
%     G_new = D*G_OO;
%     alpha = (G_new*inv(G_new^2 + 1e-1*speye(sp1)))*...
%                     (D*G_Onew);

    
    % Renormalize for numerical stability
    alpha = alpha./sum(alpha);
    
    %Square distribution, predict next observation as expectation wrt
    %squared distribution
%     Probs = (alpha).^2;
%     Probs = Probs./sum(Probs);
    nextOb = [nextOb,observations*alpha];
%     
%     alpha = mean_kbr_2(alpha,extfutures,observations,futures,1e-1,1e-1,kernelx,kernelx2,nextOb);
%     G_Onew = sv_dot(kernelx2, observations, nextOb); 

    

end
toc


