



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear all;
clc;

% Path to svlab
addpath('/Users/bboots/Documents/MATLAB/svlab');

clipStart = 0;
clipEnd = 0;
trainingData = 2500;
hankelSize = 250;
simExtent = 5000;
lowD = 500;



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Load Data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading Data');
pause(.1);
tic
load './windfarm_medium.mat';
Y = images(:,clipStart+1:end-clipEnd);
[Uy,Sy,Vy] = svd(Y(:,1:trainingData),'econ');

Uy = Uy(:,1:lowD);
Sy = Sy(1:lowD,1:lowD);
Vy = Vy(:,1:lowD);

Ytrain = Sy*Vy';
clear Y;
[d,M] = size(Ytrain);
% toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build Hankel Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Building Hankel matrix');
pause(.1);
tic
tau = M-hankelSize+1;    
X = zeros(hankelSize*d, tau);
for i = 1:hankelSize
       X((i-1)*d + 1:i*d,1:tau) = Ytrain(:,(1:tau)-1 + i);
end
M = tau;
toc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute HSE-PSR Paramters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Learning Model');
pause(.1);
tic
futures      = X(:, 1:M-1);  % Immediate future sequences of observations 
observations = X(1:d, 1:M-1);% Current observation
extfutures   = X(:,2:M);    % Future one-step-removed sequences of observations

% Find kernel bandwidth as the median of the pairwise distances (the median
% trick).

% % Bandwidth for sequences of obs
disx = fastDist(X(:,1:M),X(:,1:M));
disx = disx(:);
mdisx = median(disx);

% Gaussian RBF Kernel (for observation sequences: histories & futures)
kernelx = rbf_dot(1./mdisx);

% Bandwidth for current obs
disx2 = fastDist(observations(:,1:M-1),observations(:,1:M-1));
disx2 = disx2(:);
mdisx2 = median(disx2);

% Gaussian RBF Kernel (for current observations)
kernelx2 = rbf_dot(1./mdisx2);
%%
% Computing the Gram matrices.

tic 
lambda = 1e-1;
[n,m]=size(observations);
K = sv_dot(kernelx , futures);
K_tilda = sv_dot(kernelx , futures ,extfutures);
I = speye(m);
L = sv_dot(kernelx2 , observations);
K_p = inv(K+lambda*I);
L_p = inv(L^2+lambda*I);

toc

%%
% G_TT  = sv_dot(kernelx, futures); % Kernel matrix for tests
% G_TTT = sv_dot(kernelx, futures, extfutures); % cross kernel
% G_OO  = sv_dot(kernelx2, observations); % kernel matrix for *single* obs
% sp1 = size(G_OO,1);
%  T = inv(G_TT + 1e-1*speye(sp1))*G_TTT;
%%
% [T_alpha , T] = mean_ksr(ones(2250,1),extfutures,futures,1e-1,kernelx);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulating
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Simulating');
tic
pause(.1);

startFrame = 1000;

alpha = K(:,startFrame-1);
% alpha = alpha ./sum(alpha);

% alpha  = zeros(m,1);
% alpha(startFrame-1) = 1;
%%

%states = zeros(size(state,1),simExtent);
%  alpha = mean_new_kbr_2(alpha, observations , K_p , K_tilda ,  L_p , lambda , kernelx2 , 1000);
% G_Onew = G_OO(:,startFrame);

writerObj = VideoWriter('windsim.avi', 'Uncompressed AVI');
open(writerObj);
toc
%%
disp('loop');
tic
for i = 1:100
i
    % Kernel Bayes' Rule
    
   
%     state = T*alpha;
%     state = state./sum(state);
%     D = spdiags(state, 0, sp1, sp1);
%     G_new = D*G_OO;
%     alpha = (G_new*inv(G_new^2 + 1e-1*speye(sp1)))*...
%                     (D*G_Onew);

    
    % Renormalize for numerical stability
    alpha = alpha./sum(alpha);
    
    %Square distribution, predict next observation as expectation wrt
    %squared distribution
    Probs = (alpha).^2;
    Probs = Probs./sum(Probs);
    nextOb = observations*Probs;
    alpha = mean_new_kbr_2(alpha, observations , K_p , K_tilda ,  L_p , L , lambda , kernelx2 , nextOb);

%     G_Onew = sv_dot(kernelx2, observations, nextOb);
    
    
    % Plot predicted image
    imagesc(reshape(uint8(Uy*nextOb), x, y, z));
    axis off
    axis equal
    truesize    
    pause(.01)
    if i > 1
        frame = getframe;
        writeVideo(writerObj,frame);
    end
    

    

end
toc
close(writerObj);

