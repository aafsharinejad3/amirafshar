function [ a ] = mean_kbr_2( alpha , Y_p , X , Y , lambda ,lambda_tilda , kernel1 , kernel2 , x , K , K_x , w_no_alpha)
%UNTITLED14 Summary of this function goes here
%   Detailed explanation goes here
[n,m]=size(X);
% D = cov_ksr( alpha , Y_p , Y , lambda , kernel1);
% [B , K , K_x] = mean_kce(X , lambda , x , kernel2);
w = w_no_alpha*alpha;
D = spdiags(w,0,size(Y,2),size(Y,2));
I = speye(m);
int = inv((D*K)^2+lambda_tilda*I);
a = D*K*int*D*K_x;
end

