function [ D ] = fastDist(a, b)

dot_a = sum(a.*a,1)';
dot_b = sum(b.*b,1)';
unitvec = ones(size(a,2),1);
D = a' * b;
for i=1:size(b,2)
    D(:,i) = (-2*D(:,i) + dot_a + dot_b(i) * unitvec);
end

end

