%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Hilbert Space Embeddings of HMMs
%
% Author: Byron Boots
% Last Updated: 10/26/12
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



clear all;
close all;
clc;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add your path to svlab (http://alex.smola.org/data/svlab.tgz) here:
addpath('/Users/beb/Documents/Matlab/spectral_hmm/svlab');

% Training Parameters
remove     = 1000; % Throw away some of the initial data
m          = 2000; % Number of training data points
hankelSize = 150;  % Length of futures and histories
stateDim   = 50;   % Model dimensionality

% Testing Parameters
filterExtent   = 500;  % Pre-filter for this extent before predicting
testHorizon    = 500;  % Predict for this extent
testIterations = 2000; % Trials
testDataSize   = filterExtent + testHorizon + testIterations;

totalData = testDataSize + m;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load Slotcar Data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading data');
pause(.1);
load -ASCII 110909
Y = [X110909(:,7:9)'; X110909(:,17:19)'];
Y = Y(:,remove:remove+totalData);

% Mean-center observations
Ymean = mean(Y,2);
Y = Y - repmat(Ymean,1,size(Y,2));
[d,N] = size(Y);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build Hankel Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Building Hankel matrix');
pause(.1);

tau = N-hankelSize+1;
x = zeros(hankelSize*d, tau);
for i = 1:hankelSize
    x((i-1)*d + 1:i*d,1:tau) = Y(:,(1:tau)-1 + i);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute HSE-HMM Paramters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Learning Model');
pause(.1);
histories    = x(:, 1:m-hankelSize-1);  % Past sequences of observations
futures      = x(:, hankelSize+1:m-1);  % Immediate future sequences of observations
observations = x(1:d, hankelSize+1:m-1);% Current observation
extfutures   = x(:, hankelSize+2:m);    % Future one-step-removed sequences of observations
tY           = Y(:,m+1:end);            % Test observation data

% Find kernel bandwidth as the median of the pairwise distances (the median
% trick). Modified: increased bandwidth to make learned model more stable.

% Bandwidth for sequences of obs
disx = pdist(x(:,1:m)').^2;
mdisx = median(disx);

% Gaussian RBF Kernel (for observation sequences: histories & futures)
kernelx = rbf_dot(1./(1e+1*mdisx));

% Bandwidth for current obs
disx2 = pdist(observations').^2;
mdisx2 = median(disx2);

% Gaussian RBF Kernel (for current observations)
kernelx2 = rbf_dot(1./(1e+1*mdisx2));


% Computing the Gram matrices.
G_HH  = sv_dot(kernelx, histories); % Kernel matrix for histories
G_TT  = sv_dot(kernelx, futures); % Kernel matrix for tests
G_TTT = sv_dot(kernelx, futures, extfutures); % cross kernel
G_OO  = sv_dot(kernelx2, observations); % kernel matrix for *single* obs


% Kernel SVD to find the state space
[A, Omega] = eigs(G_HH*G_TT, stateDim);
Omega = abs(Omega); % (positive semidefinite, so shouldn't be neg.)
ALA = A' * G_TT * A;
D = diag(1./sqrt(diag(ALA)));
AD = A * D;
clear D A;


% Compute Parameters
N2    = size(G_OO,1);
part1 = G_HH * ((G_TT * AD) * diag(1./diag(Omega)));
part2 = AD'*G_TTT;
iG_OO = inv(G_OO + m*speye(N2)); % inverse + regularization

stationary  = ((AD'*G_TT)/G_HH)*(G_HH*ones(N2,1)./N2);
normalizer  = ones(1,N2)*part1;
transition  = part2*part1;
obsFunction = observations*part1;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pre-Compute Paramters for Testing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
G_Onew = (real(sv_dot(kernelx2, observations, tY))); % test data corresponding to \Phi\tps \phi(new_test_point)
update = iG_OO*G_Onew;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Filtering and Predicting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Filtering and Predicting');
pause(.1);

% Mean Frame (baseline)
mframe = mean(Y(:,1:m),2);

% Prediction Errors
err  = zeros(testIterations-filterExtent+1,testHorizon);
err2 = zeros(testIterations-filterExtent+1,testHorizon);

% States
state   = stationary;
states  = zeros(size(state,1),testIterations);

sp1  = size(part1,1);
ii   = 0;

for i = 1:testIterations+filterExtent-1
    
    % HSE-HMM state update (filter based on current observation)
    state = part2*(spdiags(update(:,i), 0, sp1, sp1)*(part1*state));
    state = state./(normalizer*state);
    states(:,i) = state;
    
    if (i > filterExtent)
        expectedState = state;
        ii = i - filterExtent;
        
        % Run the system testHorizon steps forward without an observation;
        for j = 1:testHorizon
            
            % Calculate expected future obs as a linear function of state
            predFrame = obsFunction*expectedState;
            
            % Sum-squared error for predicted frame (err) and average frame
            err(ii,j)  = sum((predFrame - tY(:,i+j-1)).^2);
            err2(ii,j) = sum((mframe - tY(:,i+j-1)).^2);
            
            % Calculate next expected state
            expectedState = transition*expectedState;
        end 
    end
end

% Final Results
figure;
plot(mean(err), 'r-'); % Model Prediction Error
hold
plot(mean(err2), 'g-'); % Baseline Error (predict using mean frame)

