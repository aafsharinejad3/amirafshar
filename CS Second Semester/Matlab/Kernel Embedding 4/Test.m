%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load Slotcar Data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 1.  FrameId
% 2.  SrvTime
% 3.  ClientTime
% 4.  PixX
% 5.  PixY
% 6.  UnfilteredLoc
% 7.  Roll
% 8.  Pitch
% 9.  Yaw
% 10. AngleFromIMU
% 11. FilteredLoc
% 12. FilteredVel
% 13. AngleFromVisionAndModel
% 14. sin(angle_vis)
% 15. cos(angle_vis)
% 16. Control_Signal(0-255)
% 17-19. 3AccelerometerValues

disp('Loading data');


load -ASCII 112109
Y = X112109(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;
U = X112109(:,16)'./255;
figure(1)
plot(Y)
hold
plot(U, 'r')
clear X112109;


%% Testing

% Forming Hankel matrix + training and testing sets
% We want to calculate the conditional mean of Y(FilteredLoc) given
% X(control signal) for 4 previous states.
y = Y(5:end);
x = zeros(4,length(y));
for i = 1:size(x,2)
   x(:,i) = (U(i:i+3))'; 
end

y_train = y(1:floor(length(y)*0.8));
x_train = x(:,1:floor(length(y)*0.8));
y_test = y(length(y_train)+5:end);
x_test = x(:,length(y_train)+5:end);

disx = pdist(x_train').^2;
mdisx = median(disx);

% Gaussian RBF Kernel (for observation sequences: histories & futures)
kernelx = rbf_dot(1./(1e+1*mdisx));

[ B , K , K_x ] = mean_kce(x_train,0.025,x_test,kernelx);
result = y_train*B;
err = mean(y_test-result);