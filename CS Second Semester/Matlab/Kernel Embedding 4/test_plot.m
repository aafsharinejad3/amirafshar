
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Initializations
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear all;
clc;
% 
addpath('\Users\lab\Desktop\CS First Semester\Matlab\SVLab\svlab');
% Path to svlab
clipStart = 0;
clipEnd = 0;
trainingData = 2500;
hankelSize = 250;
simExtent = 5000;
lowD = 500;

% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Load Data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading Data');
pause(.1);
load -ASCII 110909_2
Y = X110909_2(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;

Ytrain = Y(1:floor(0.8*8403));
Ytest = Y(floor(0.8*8403)+1:end);
[d,M] = size(Ytrain);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build Hankel Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Building Hankel matrix');
pause(.1);
tic
tau = M-hankelSize+1;    
X = zeros(hankelSize*d, tau);
for i = 1:hankelSize
       X((i-1)*d + 1:i*d,1:tau) = Ytrain(:,(1:tau)-1 + i);
end
M = tau;
toc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute HSE-PSR Paramters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Learning Model');
pause(.1);
tic
futures      = X(:, 1:M-1);  % Immediate future sequences of observations 
observations = X(1:d, 1:M-1);% Current observation
extfutures   = X(:,2:M);    % Future one-step-removed sequences of observations



load('next_ob_reg')
nextOb_reg = nextOb;
load('next_ob_irreg')
nextOb_irreg = zeros(1,100);
nextOb_irreg(1:40) = nextOb;
figure()
plot(linspace(1,100),nextOb_reg,'b*',linspace(1,100),nextOb_irreg,'r*',linspace(1,100),Ytest(1:100),'g*');