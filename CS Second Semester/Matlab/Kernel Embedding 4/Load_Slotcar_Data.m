%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Load Slotcar Data into Matlab
%
% Author: Byron Boots
% Last Updated: 8/23/14
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;
clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load Slotcar Data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 1.  FrameId
% 2.  SrvTime
% 3.  ClientTime
% 4.  PixX
% 5.  PixY
% 6.  UnfilteredLoc
% 7.  Roll
% 8.  Pitch
% 9.  Yaw
% 10. AngleFromIMU
% 11. FilteredLoc
% 12. FilteredVel
% 13. AngleFromVisionAndModel
% 14. sin(angle_vis)
% 15. cos(angle_vis)
% 16. Control_Signal(0-255)
% 17-19. 3AccelerometerValues

disp('Loading data');

load -ASCII 110909
Y = X110909(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;
U = X110909(:,16)'./255;
figure(1)
plot(Y)
hold
plot(U, 'r')
clear X110909;

load -ASCII 110909_2
Y = X110909_2(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;
U = X110909_2(:,16)'./255;
figure(2)
plot(Y)
hold
plot(U, 'r')
clear X110909_2;

load -ASCII 112009
Y = X112009(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;
U = X112009(:,16)'./255;
figure(3)
plot(Y)
hold
plot(U, 'r')
clear X112009;

load -ASCII 112109
Y = X112109(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;
U = X112109(:,16)'./255;
figure(4)
plot(Y)
hold
plot(U, 'r')
clear X112109;

load -ASCII 112509
Y = X112509(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;
U = X112509(:,16)'./255;
figure(5)
plot(Y)
hold
plot(U, 'r')
clear X112509;

load -ASCII 120109
Y = X120109(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;
U = X120109(:,16)'./255;
figure(6)
plot(Y)
hold
plot(U, 'r')
clear X120109;



