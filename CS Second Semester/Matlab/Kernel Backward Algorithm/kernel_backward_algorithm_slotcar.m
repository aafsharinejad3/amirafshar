% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Initializations
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear all;
clc;
% 
% Path to svlab
addpath('../SVLab/svlab');

clipStart = 0;
clipEnd = 0;
trainingData = 1000;
hankelSize = 250;
simExtent = 5000;
lowD = 500;

% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Load Data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading Data');
pause(.1);
load -ASCII 110909_2
%%
X6 = X110909_2(:,6)';%Unfiltered location on track between (-1 and 1365.7)
X11 = X110909_2(:,11)';%Filtered location on track (0.15,1365.6)
X4 = X110909_2(:,4)';%x position maybe
X5 = X110909_2(:,5)';%y position maybe 
Z7 = X110909_2(:,7)';%IMU data(noisy angles and acceleration)(-3069,2758)
Z8 = X110909_2(:,8)';%IMU data(noisy angles and acceleration)(-3845,1798)
Z9 = X110909_2(:,9)';%IMU data(noisy angles and acceleration)(-7649,7192)
Z17 = X110909_2(:,17)';%IMU data(noisy angles and acceleration)(-544,361)
Z18 = X110909_2(:,18)';%IMU data(noisy angles and acceleration)(-848,844)
Z19 = X110909_2(:,19)';%IMU data(noisy angles and acceleration)(-60,891)


%% Whitening

obs = [Z7;Z8;Z9;Z17;Z18;Z19];
% X11(1,:) = cos(X11./max(X11)*2*pi)./2;
% X11(2,:) = sin(X11./max(X11)*2*pi)./2;


hid = X11;

obs_train_nw = obs(:,1:7000);
hid_train_nw = hid(:,1:7000);



sigma_obs_train = cov(obs_train_nw');
sigma_hid_train = cov(hid_train_nw);
[U,S,V] = svd(sigma_obs_train);
[U1,S1,V1] = svd(sigma_hid_train);
% 
obs_train = S^(-0.5)*U'*obs_train_nw;
% hid_train = S1^(-0.5)*hid_train_nw;
% obs_train = obs_train_nw;
hid_train = hid_train_nw;

hid_train2(1,:) = cos(hid_train./max(hid_train)*2*pi)./2 ;% mapped on 0-1 scale after whitening
hid_train2(2,:) = sin(hid_train./max(hid_train)*2*pi)./2 ;


obs_test_nw = obs(:,7001:8403);
hid_test_nw = hid(:,7001:8403);
sigma_obs_test = cov(obs_test_nw');
sigma_hid_test = cov(hid_test_nw);
[U2,S2,V2] = svd(sigma_obs_test);
[U3,S3,V3] = svd(sigma_hid_test);

obs_test = S2^(-0.5)*U2'*obs_test_nw;
% hid_test = S3^(-0.5)*hid_test_nw;
% obs_test = obs_test_nw;
hid_test = hid_test_nw;


hid_test2(1,:) = cos(hid_test./max(hid_test)*2*pi)./2 ;% mapped on 0-1 scale after whitening
hid_test2(2,:) = sin(hid_test./max(hid_test)*2*pi)./2 ;
%%
%  load('train_test_sets.mat')

M = 7000 - 1;
hidden = hid_train2(:,1:end-1);
next_hidden = hid_train2(:,2:end);
observ = obs_train(:,1:end-1);

%%
% Find kernel bandwidth as the median of the pairwise distances (the median
% trick).

% % Bandwidth for sequences of obs
disx = fastDist(hid_train2,hid_train2);
disx = disx(:);
mdisx = median(disx)/4;

% Gaussian RBF Kernel (for observation sequences: histories & futures)
kernelx = rbf_dot(1./mdisx);

% Bandwidth for current obs
disx2 = fastDist(observ,observ);
disx2 = disx2(:);
mdisx2 = 4*median(disx2);

% Gaussian RBF Kernel (for current observations)
kernelx2 = rbf_dot(1./mdisx2);


%%

lambda = 0.01;
lambda_tilda = 0.01;
startFrame = 1000;
[B , K , K_x] = mean_kce(observ , lambda , obs_test(:,:) , kernelx2);
alpha = K(:,startFrame-1);
[w w_no_alpha] = mean_ksr( alpha , next_hidden , hidden , lambda , kernelx);
[etha_t_ish, G_x, G_x_pr, G_x_pr_x] = kba_pre_calc(next_hidden , hidden , lambda ,lambda_tilda , kernelx);

%%

nextOb = [];
nextHid = [];
alpha_b = [];
for i = 1:length(hid_test)
i
     tic
    % Kernel Bayes' Rule
    alpha = mean_kbr_2(alpha,next_hidden,observ,hidden,lambda,lambda,kernelx,kernelx2,obs_test(:,i),K,K_x(:,i),w_no_alpha);
    toc
    
    % Renormalize for numerical stability
    alpha = alpha./sum(alpha);
    
    %Square distribution, predict next observation as expectation wrt
    %squared distribution
%     Probs = (alpha).^2;
%     Probs = Probs./sum(Probs);
    nextOb = [nextOb,observ*alpha];
    nextHid = [nextHid,hidden*alpha];
    alpha_b = [alpha_b,alpha];
%     
%     alpha = mean_kbr_2(alpha,extfutures,observations,futures,1e-1,1e-1,kernelx,kernelx2,nextOb);
%     G_Onew = sv_dot(kernelx2, observations, nextOb); 

    

end


%%
% figure()
% plot(linspace(1,length(nextOb),length(nextOb)),nextOb(1,1:500),'*r',linspace(1,500,500),obs_test(1,1:500),'*g');
% figure()
% plot(linspace(1,length(nextHid),length(nextHid)),nextHid(1,1:500),'*r',linspace(1,500,500),hid_test2(1,1:500),'*g');
% figure()
% plot(linspace(1,length(nextHid),length(nextHid)),nextHid(2,1:500),'*r',linspace(1,500,500),hid_test2(2,1:500),'*g');

%%
% figure()
% text(100,100,'6d ')
% subplot(2,2,1)
% plot(nextHid(1,1:125),nextHid(2,1:125),'*r',hid_test2(1,1:125),hid_test2(2,1:125),'*g');
% title('1:125')
% subplot(2,2,2)
% plot(nextHid(1,126:250),nextHid(2,126:250),'*r',hid_test2(1,126:250),hid_test2(2,126:250),'*g');
% title('126:250')
% subplot(2,2,3)
% plot(nextHid(1,251:375),nextHid(2,251:375),'*r',hid_test2(1,251:375),hid_test2(2,251:375),'*g');
% title('251:375')
% subplot(2,2,4)
% plot(nextHid(1,376:500),nextHid(2,376:500),'*r',hid_test2(1,376:500),hid_test2(2,376:500),'*g');
% title('376:500')

%%

alpha_f = alpha_b(:,1403);
w_all = [];
prediction = [];
prediction_hid = [];
gamma = [];
    disp('gamma');

prediction = zeros(6,1402);
prediction_hid = zeros(2,1402);

for j = 1402:-1:1
   
    tic
    gamma_t  = kba_amir( alpha_b(:,j) , 0.01 ,0.01 , kernelx , etha_t_ish, G_x, G_x_pr, G_x_pr_x);
    %gamma = [gamma,gamma_t];
    alpha_f = gamma_t * alpha_f; 
   
    alpha_n = alpha_f./sum(alpha_f);
    prediction(:,j) = observ*alpha_n;
    prediction_hid(:,j) = hidden*alpha_n;
    %prediction = [prediction, observ*alpha_n];
   % prediction_hid = [prediction_hid, hidden*alpha_n];
    %w_all = [w_all,alpha_n];
    toc
end

%%
mse1 = mean((prediction_hid(1,1:200)-hid_test2(1,1:200)).^2);
mse2 = mean((prediction_hid(2,1:200)-hid_test2(2,1:200)).^2);
mse3 = mean((prediction_hid(1,201:400)-hid_test2(1,201:400)).^2);
mse4 = mean((prediction_hid(2,201:400)-hid_test2(2,201:400)).^2);
mse5 = mean((prediction_hid(1,401:600)-hid_test2(1,401:600)).^2);
mse6 = mean((prediction_hid(2,401:600)-hid_test2(2,401:600)).^2);
mse7 = mean((prediction_hid(1,601:800)-hid_test2(1,601:800)).^2);
mse8 = mean((prediction_hid(2,601:800)-hid_test2(2,601:800)).^2);
mse9 = mean((prediction_hid(1,801:1000)-hid_test2(1,801:1000)).^2);
mse10 = mean((prediction_hid(2,801:1000)-hid_test2(2,801:1000)).^2);
mse11 = mean((prediction_hid(1,1001:1400)-hid_test2(1,1001:1400)).^2);
mse12 = mean((prediction_hid(2,1001:1400)-hid_test2(2,1001:1400)).^2);

figure()
text(100,100,'6d ')
subplot(2,3,1)
plot(prediction_hid(1,1:200),prediction_hid(2,1:200),'*r',hid_test2(1,1:200),hid_test2(2,1:200),'*g');
d1 = sprintf('1:200 - mse = (%f - %f)',mse1,mse2)
title(d1)
subplot(2,3,2)
plot(prediction_hid(1,201:400),prediction_hid(2,201:400),'*r',hid_test2(1,201:400),hid_test2(2,201:400),'*g');
d1 = sprintf('201:400 - mse = (%f - %f)',mse3,mse4)
title(d1)
subplot(2,3,3)
plot(prediction_hid(1,401:600),prediction_hid(2,401:600),'*r',hid_test2(1,401:600),hid_test2(2,401:600),'*g');
d1 = sprintf('401:600 - mse = (%f - %f)',mse5,mse6)
title(d1)
subplot(2,3,4)
plot(prediction_hid(1,601:800),prediction_hid(2,601:800),'*r',hid_test2(1,601:800),hid_test2(2,601:800),'*g');
d1 = sprintf('601:800 - mse = (%f - %f)',mse7,mse8)
title(d1)
subplot(2,3,5)
plot(prediction_hid(1,801:1000),prediction_hid(2,801:1000),'*r',hid_test2(1,801:1000),hid_test2(2,801:1000),'*g');
d1 = sprintf('801:1000 - mse = (%f - %f)',mse9,mse10)
title(d1)
subplot(2,3,6)
plot(prediction_hid(1,1001:1400),prediction_hid(2,1001:1400),'*r',hid_test2(1,1001:1400),hid_test2(2,1001:1400),'*g');
d1 = sprintf('1001:1400 - mse = (%f - %f)',mse11,mse12)
title(d1)
%%
% figure()
% text(100,100,'6d ')
% subplot(2,2,1)
% plot(prediction_hid(1,1:125),prediction_hid(2,1:125),'*r',hid_test2(1,1:125),hid_test2(2,1:125),'*g');
% d1 = sprintf('1:125 - mse = (%f - %f)',mse1,mse2)
% title(d1)
% subplot(2,2,2)
% plot(prediction_hid(1,126:250),prediction_hid(2,126:250),'*r',hid_test2(1,126:250),hid_test2(2,126:250),'*g');
% d1 = sprintf('126:250 - mse = (%f - %f)',mse3,mse4)
% title(d1)
% subplot(2,2,3)
% plot(prediction_hid(1,251:375),prediction_hid(2,251:375),'*r',hid_test2(1,251:375),hid_test2(2,251:375),'*g');
% d1 = sprintf('251:375 - mse = (%f - %f)',mse5,mse6)
% title(d1)
% subplot(2,2,4)
% plot(prediction_hid(1,376:499),prediction_hid(2,376:499),'*r',hid_test2(1,376:499),hid_test2(2,376:499),'*g');
% d1 = sprintf('376:499 - mse = (%f - %f)',mse7,mse8)
title(d1)


%%



figure()
plot(linspace(1,length(prediction),length(prediction)),prediction(1,:),'*r',linspace(1,1402,1402),obs_test(1,1:1402),'*g');

figure()
plot(linspace(1,length(prediction_hid),length(prediction_hid)),prediction_hid(1,:),'*r',linspace(1,1402,1402),hid_test2(1,1:1402),'*g');


figure()
plot(linspace(1,length(prediction_hid),length(prediction_hid)),prediction_hid(2,:),'*r',linspace(1,1402,1402),hid_test2(2,1:1402),'*g');

%%
% figure() 
% imagesc(G_x)

% %%
% figure()
% 
% plot(hid_train2(1,:),hid_train2(2,:))
