function [ a ] = mean_ke( X )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if (nargin < 1)
disp('Please check the function input argument. This function takes only one input X(n,m), where "m" is the number of data points and "n" is the number of dimensions.')
return
end
[n,m]=size(phi);
a=1/m.*ones(1,m);

end