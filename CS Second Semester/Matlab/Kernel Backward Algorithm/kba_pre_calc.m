function [ etha_t_ish, G_x, G_x_pr, G_x_pr_x ] = kba_pre_calc(Y_p , Y , lambda ,lambda_tilda , kernel1)

G_x_pr = sv_dot(kernel1,Y_p);
G_x = sv_dot(kernel1,Y);
G_x_pr_x = sv_dot (kernel1,Y_p,Y);
I = speye(size(G_x,2));
etha_t_ish = inv(G_x + lambda*I)*G_x;

end