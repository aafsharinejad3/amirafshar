% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Initializations
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear all;
clc;
% 
% Path to svlab
addpath('../SVLab/svlab');
addpath('KalmanAll/Kalman')
addpath('KalmanAll/KPMtools')
addpath('KalmanAll/KPMstats')
clipStart = 0;
clipEnd = 0;
trainingData = 1000;
hankelSize = 250;
simExtent = 5000;
lowD = 500;

% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Load Data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading Data');
pause(.1);
load -ASCII 110909_2
%%
X6 = X110909_2(:,6)';%Unfiltered location on track between (-1 and 1365.7)
X11 = X110909_2(:,11)';%Filtered location on track (0.15,1365.6)
X4 = X110909_2(:,4)';%x position maybe
X5 = X110909_2(:,5)';%y position maybe 
Z7 = X110909_2(:,7)';%IMU data(noisy angles and acceleration)(-3069,2758)
Z8 = X110909_2(:,8)';%IMU data(noisy angles and acceleration)(-3845,1798)
Z9 = X110909_2(:,9)';%IMU data(noisy angles and acceleration)(-7649,7192)
Z17 = X110909_2(:,17)';%IMU data(noisy angles and acceleration)(-544,361)
Z18 = X110909_2(:,18)';%IMU data(noisy angles and acceleration)(-848,844)
Z19 = X110909_2(:,19)';%IMU data(noisy angles and acceleration)(-60,891)


%% Whitening

obs = [Z7;Z8;Z9;Z17;Z18;Z19];
% X11(1,:) = cos(X11./max(X11)*2*pi)./2;
% X11(2,:) = sin(X11./max(X11)*2*pi)./2;


hid = X11;

obs_train_nw = obs(:,1:7000);
hid_train_nw = hid(:,1:7000);



sigma_obs_train = cov(obs_train_nw');
sigma_hid_train = cov(hid_train_nw);
[U,S,V] = svd(sigma_obs_train);
[U1,S1,V1] = svd(sigma_hid_train);
% 
obs_train = S^(-0.5)*U'*obs_train_nw;
% hid_train = S1^(-0.5)*hid_train_nw;
% obs_train = obs_train_nw;
hid_train = hid_train_nw;

hid_train2(1,:) = cos(hid_train./max(hid_train)*2*pi)./2 ;% mapped on 0-1 scale after whitening
hid_train2(2,:) = sin(hid_train./max(hid_train)*2*pi)./2 ;


obs_test_nw = obs(:,7001:8403);
hid_test_nw = hid(:,7001:8403);
sigma_obs_test = cov(obs_test_nw');
sigma_hid_test = cov(hid_test_nw);
[U2,S2,V2] = svd(sigma_obs_test);
[U3,S3,V3] = svd(sigma_hid_test);

obs_test = S2^(-0.5)*U2'*obs_test_nw;
% hid_test = S3^(-0.5)*hid_test_nw;
% obs_test = obs_test_nw;
hid_test = hid_test_nw;


hid_test2(1,:) = cos(hid_test./max(hid_test)*2*pi)./2 ;% mapped on 0-1 scale after whitening
hid_test2(2,:) = sin(hid_test./max(hid_test)*2*pi)./2 ;
%%
%  load('train_test_sets.mat')

M = 7000 - 1;
hidden = hid_train2(:,1:end-1);
next_hidden = hid_train2(:,2:end);
observ = obs_train(:,1:end-1);

%% Linear Regression for Kalman Filtering

A = mvregress(hidden',next_hidden');
nexx = A'*hidden;
eps = next_hidden-nexx;
cov_eps = cov(eps');

C = mvregress(hidden',observ');
oobs = C'*hidden;
ksi = observ-oobs;
cov_ksi = cov(ksi');

%% Kalman filtering

[hid_kalman, cov_kalman, VV_kalman, loglik] = kalman_filter(obs_test,A',C',cov_eps,cov_ksi,mean(hidden,2),cov(hidden'));

%% Kalman Smoothing
[sm_hid_kalman, sm_cov_kalman, sm_VV_kalman, sm_loglik] = kalman_smoother(obs_test,A',C',cov_eps,cov_ksi,mean(hidden,2),cov(hidden'));

%%
figure()
subplot(1,2,1)
plot(hid_kalman(1,1:300),hid_kalman(2,1:300),'g*',hid_kalman(1,301:600),hid_kalman(2,301:600),'r*',hid_kalman(1,601:900),hid_kalman(2,601:900),'b*',hid_kalman(1,901:1200),hid_kalman(2,901:1200),'m*',hid_kalman(1,1201:1403),hid_kalman(2,1201:1403),'k*');
ylim([-0.5 0.5]);
legend('1:300','301:600','601:900','901:1200','1201:1403')
title('Kalman filter-10*Q')
subplot(1,2,2)
plot(hid_test2(1,1:300),hid_test2(2,1:300),'g*',hid_test2(1,301:600),hid_test2(2,301:600),'r*',hid_test2(1,601:900),hid_test2(2,601:900),'b*',hid_test2(1,901:1200),hid_test2(2,901:1200),'m*',hid_test2(1,1201:1403),hid_test2(2,1201:1403),'k*');

%%

figure()
subplot(1,2,1)
plot(sm_hid_kalman(1,1:300),sm_hid_kalman(2,1:300),'g*',sm_hid_kalman(1,301:600),sm_hid_kalman(2,301:600),'r*',sm_hid_kalman(1,601:900),sm_hid_kalman(2,601:900),'b*',sm_hid_kalman(1,901:1200),sm_hid_kalman(2,901:1200),'m*',sm_hid_kalman(1,1201:1403),sm_hid_kalman(2,1201:1403),'k*');
ylim([-0.5 0.5]);
legend('1:300','301:600','601:900','901:1200','1201:1403')
title('Kalman smoother-I')
subplot(1,2,2)
plot(hid_test2(1,1:300),hid_test2(2,1:300),'g*',hid_test2(1,301:600),hid_test2(2,301:600),'r*',hid_test2(1,601:900),hid_test2(2,601:900),'b*',hid_test2(1,901:1200),hid_test2(2,901:1200),'m*',hid_test2(1,1201:1403),hid_test2(2,1201:1403),'k*');