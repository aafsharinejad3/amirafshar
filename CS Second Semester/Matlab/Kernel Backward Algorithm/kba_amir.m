function [ gamma_t ] = kba_amir( alpha , lambda ,lambda_tilda , kernel1 , etha_t_ish, G_x, G_x_pr, G_x_pr_x)

etha_t = etha_t_ish*alpha;
D_etha_t = spdiags(etha_t,0,size(etha_t,1),size(etha_t,1));
I = speye(size(D_etha_t*G_x_pr));
gamma_t = D_etha_t*G_x_pr*inv((D_etha_t*G_x_pr)^2 + lambda_tilda*I)*D_etha_t*G_x_pr_x;


end