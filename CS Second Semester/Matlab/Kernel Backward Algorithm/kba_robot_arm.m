clear all
close all
load('data_Jul 5 14 16\data_Jul  5 14_46_27.mat');
addpath('data_Jul 5 14 16\');

%%
rgb_ts = rgbImg.timestamp;
rgb_ts_edit = rgb_ts(1:7150);
cut = wamData.wamjointangles_commanded(1001:end-1000,:);
ang = cut;
ang_ts = wamData.timestamp(1001:end-1000,:);
vel = (wamData.wamjointangles_commanded(1002:end-1000+1,:)-wamData.wamjointangles_commanded(1000:end-1001,:))./2;
meas = (wamData.wamjointangles_commanded(1002:end-1000+1,:)-wamData.wamjointangles_commanded(1001:end-1000,:));
final_ang_ts = zeros(7150,1);
final_angle = zeros(7150,7);
final_vel = zeros(7150,7);
%%
lo = [];
loo = [];
temp = ang_ts;
for i = 1:7150
%     [s,idx] = getNElements((rgb_ts_edit(i)-ang_ts), 2);
%     if idx(1) ~= temp
   
   [c index] = min(abs(rgb_ts_edit(i)-temp)) ;
   idx = find(ang_ts == temp(index));
   final_ang_ts(i) = ang_ts(idx);
   final_angle(i,:) = ang(idx,:);
   final_vel(i,:) = vel(idx,:);
    lo = [lo,index];
    loo = [loo,idx];
     temp = temp([1:index-1,index+1:end]);
%    idx(1) = temp;
%     else
%    final_ang_ts(i) = ang_ts(idx(2));
%    final_angle(i,:) = ang(idx(2),:);
%     end
  
end
%%
crit = final_angle(2:end,:)-final_angle(1:end-1,:);
elim = find(abs(crit(:,4))> 5*abs(median(meas(:,4))));


elim = [0,elim'];
learn = zeros(length(elim)-1,1274);
learn_vel = zeros(length(elim)-1,1274);
up_idx = [];
low_idx = [];
for i = 1:length(elim)-1
   if elim(i+1)-elim(i)>100
       low_idx = [low_idx,elim(i)+1];
       up_idx = [up_idx,elim(i+1)-1];
       learn(i,1:length(elim(i)+1:elim(i+1))) = final_angle(elim(i)+1:elim(i+1),4);
       learn_vel(i,1:length(elim(i)+1:elim(i+1))) = final_vel(elim(i)+1:elim(i+1),4);
   else
       learn(i,:) = zeros(1,1274);
       learn_vel(i,:) = zeros(1,1274);
   end
    
    
end

 learn(all(learn==0,2),:)=[];
 learn_vel(all(learn_vel==0,2),:)=[];
%%
% 
 load('data_rgb');
% data_rgb = zeros(480*0.3,640*0.3,3,7150);
% for k = 1:7150
% 	% Create a mat filename, and load it into a structure called matData.
% 	matFileName = sprintf('rgb_%d.mat', k);
% 	if exist(matFileName, 'file')
% 		matData = load(matFileName);
%         data_rgb(:,:,:,k) = imresize(matData.image,0.3);
% 	else
% 		fprintf('File %s does not exist.\n', matFileName);
% 	end
% end	
% % 
% % for k = 931:7000
% % 	% Create a mat filename, and load it into a structure called matData.
% % 	matFileName = sprintf('depth_%d.mat', k);
% % 	if exist(matFileName, 'file')
% % 		matData = load(matFileName);
% %         data(:,:,k-1) = matData.image;
% % 	else
% % 		fprintf('File %s does not exist.\n', matFileName);
% % 	end
% % end
% 
%  %%
%   save('data_rgb.mat','data_rgb' ,'-v7.3') ;

%%
 data_rgb_conc = reshape(data_rgb,size(data_rgb,1)*size(data_rgb,2),3,7150);
 data = reshape(data_rgb_conc,size(data_rgb_conc,1)*size(data_rgb_conc,2),7150) ;
 
 %%
%  [U1,S1,V1] = svds(data(:,1:3:end),300);
load('U1');
load('V1');
load('S1');
 %%
 I_red = U1'*data;
 [U2,S2,V2] = svds(I_red,200);
 
 %% Final Data
 l = length(final_angle);
 Y = (S2^0.5)*V2';
 img_learn = zeros(200,23,1274);
 
 for j =1:23
    
     img_learn(:,j,1:up_idx(j)-low_idx(j)+2) = Y(:,low_idx(j):up_idx(j)+1); 
     
 end
 %%
 
%  observ = Y(:,1:0.8*l-1);
%  obs_test = Y(:,0.8*l+1:end);
%  final_angle_c(1,:) = cos(final_angle(:,4)./max(final_angle(:,4))*2*pi)./2 ;
%  final_angle_c(2,:) = sin(final_angle(:,4)./max(final_angle(:,4))*2*pi)./2 ;
%  hidden = final_angle(1:0.8*l-1,4)';
%  next_hidden = final_angle(2:0.8*l,4)';
%  hid_test = final_angle(0.8*l+1:end,4)';
%  hidden = final_angle_c(:,1:0.8*l-1);
%  next_hidden = final_angle_c(:,2:0.8*l);
%  hid_test = final_angle_c(:,0.8*l+1:end);
 hidden = zeros(2,(nnz(learn(1:18,:))-18));
 next_hidden = zeros(2,(nnz(learn(1:18,:))-18));
 observ = zeros(200,(nnz(learn(1:18,:))-18));
 temp = 0;
 for i = 1:18
    temp_l = temp; 
    temp = temp + nnz(learn(i,:))-1; 
    nnz_idx = find(learn(i,:)>0);
    hidden(1,temp_l+1:temp) = learn(i,nnz_idx(1:end-1));
    hidden(2,temp_l+1:temp) = learn_vel(i,nnz_idx(1:end-1));
    next_hidden(1,temp_l+1:temp) = learn(i,nnz_idx(2:end));
    next_hidden(2,temp_l+1:temp) = learn_vel(i,nnz_idx(2:end));
    observ (:,temp_l+1:temp) = img_learn(:,i,nnz_idx(1:end-1));
 end
 
 hid_test = zeros(2,nnz(learn(19:23,:)));
 obs_test = zeros(200,nnz(learn(19:23,:)));
 
 temp_test = 0;
 
 for j = 19:23
     
     temp_test_l = temp_test;
     temp_test = temp_test + nnz(learn(j,:));
     nnz_idx = find(learn(j,:)>0);
     hid_test(1,temp_test_l+1:temp_test) = learn(j,nnz_idx);
     hid_test(2,temp_test_l+1:temp_test) = learn_vel(j,nnz_idx);
     obs_test(:,temp_test_l+1:temp_test) = img_learn(:,j,nnz_idx);
 end
 
 %%
 clear data_rgb
 clear data_rgb_conc
 
 %%
 addpath('../SVLab/svlab');
 % Find kernel bandwidth as the median of the pairwise distances (the median
% trick).

% % Bandwidth for sequences of obs
disx = fastDist(hidden,hidden);
disx = disx(:);
mdisx = median(disx)/4;

% Gaussian RBF Kernel (for observation sequences: histories & futures)
kernelx = rbf_dot(1./mdisx);

% Bandwidth for current obs
disx2 = fastDist(observ,observ);
disx2 = disx2(:);
mdisx2 = 4*median(disx2);

% Gaussian RBF Kernel (for current observations)
kernelx2 = rbf_dot(1./mdisx2);


%%

lambda = 0.01;
lambda_tilda = 0.01;
startFrame = 1000;
[B , K , K_x] = mean_kce(observ , lambda , obs_test(:,:) , kernelx2);
alpha = K(:,startFrame-1);
[w w_no_alpha] = mean_ksr( alpha , next_hidden , hidden , lambda , kernelx);
[etha_t_ish, G_x, G_x_pr, G_x_pr_x] = kba_pre_calc(next_hidden , hidden , lambda ,lambda_tilda , kernelx);

%%

nextOb = zeros(size(obs_test));
nextHid = zeros(size(hid_test));
alpha_b = zeros(5069,length(hid_test));
for i = 1:length(hid_test)
i
     tic
    % Kernel Bayes' Rule
    alpha = mean_kbr_2(alpha,next_hidden,observ,hidden,lambda,lambda,kernelx,kernelx2,obs_test(:,i),K,K_x(:,i),w_no_alpha);
    toc
    
    % Renormalize for numerical stability
    alpha = alpha./sum(alpha);
    
    %Square distribution, predict next observation as expectation wrt
    %squared distribution
%     Probs = (alpha).^2;
%     Probs = Probs./sum(Probs);
%     nextOb = [nextOb,observ*alpha];
%     nextHid = [nextHid,hidden*alpha];
%     alpha_b = [alpha_b,alpha];
    nextOb(:,i) = observ*alpha;
    nextHid(:,i) = hidden*alpha;
    alpha_b(:,i) = alpha;
%     
%     alpha = mean_kbr_2(alpha,extfutures,observations,futures,1e-1,1e-1,kernelx,kernelx2,nextOb);
%     G_Onew = sv_dot(kernelx2, observations, nextOb); 

    

end



% figure()
% plot(linspace(1,length(nextOb),length(nextOb)),nextOb(1,1:500),'*r',linspace(1,500,500),obs_test(1,1:500),'*g');
% figure()
% plot(linspace(1,length(nextHid),length(nextHid)),nextHid(1,1:500),'*r',linspace(1,500,500),hid_test2(1,1:500),'*g');
% figure()
% plot(linspace(1,length(nextHid),length(nextHid)),nextHid(2,1:500),'*r',linspace(1,500,500),hid_test2(2,1:500),'*g');


% figure()
% text(100,100,'6d ')
% subplot(2,2,1)
% plot(nextHid(1,1:125),nextHid(2,1:125),'*r',hid_test2(1,1:125),hid_test2(2,1:125),'*g');
% title('1:125')
% subplot(2,2,2)
% plot(nextHid(1,126:250),nextHid(2,126:250),'*r',hid_test2(1,126:250),hid_test2(2,126:250),'*g');
% title('126:250')
% subplot(2,2,3)
% plot(nextHid(1,251:375),nextHid(2,251:375),'*r',hid_test2(1,251:375),hid_test2(2,251:375),'*g');
% title('251:375')
% subplot(2,2,4)
% plot(nextHid(1,376:500),nextHid(2,376:500),'*r',hid_test2(1,376:500),hid_test2(2,376:500),'*g');
% title('376:500')
%%


alpha_f = alpha_b(:,1118);
w_all = [];
% prediction = [];
% prediction_hid = [];
gamma = [];
    disp('gamma');

prediction = zeros(size(obs_test));
prediction_hid = zeros(size(hid_test));

for j = length(hid_test)-1:-1:1
   
    tic
    gamma_t  = kba_amir( alpha_b(:,j) , 0.01 ,0.01 , kernelx , etha_t_ish, G_x, G_x_pr, G_x_pr_x);
    %gamma = [gamma,gamma_t];
    alpha_f = gamma_t * alpha_f; 
   
    alpha_n = alpha_f./sum(alpha_f);
    prediction(:,j) = observ*alpha_n;
    prediction_hid(:,j) = hidden*alpha_n;
    %prediction = [prediction, observ*alpha_n];
   % prediction_hid = [prediction_hid, hidden*alpha_n];
    %w_all = [w_all,alpha_n];
    toc
end
%%
% error = zeros(7,1);
% error_filtering = zeros(7,1);
% for k = 1:7
%    
%     error(k) = norm((prediction_hid(k,:)-hid_test(k,1:end-1)))/norm(hid_test(k,1:end-1));
%     error_filtering(k) = norm((nextHid(k,:)-hid_test(k,1:end)))/norm(hid_test(k,1:end));
% end



%%
% mse1 = mean((prediction_hid(1,1:200)-hid_test2(1,1:200)).^2);
% mse2 = mean((prediction_hid(2,1:200)-hid_test2(2,1:200)).^2);
% mse3 = mean((prediction_hid(1,201:400)-hid_test2(1,201:400)).^2);
% mse4 = mean((prediction_hid(2,201:400)-hid_test2(2,201:400)).^2);
% mse5 = mean((prediction_hid(1,401:600)-hid_test2(1,401:600)).^2);
% mse6 = mean((prediction_hid(2,401:600)-hid_test2(2,401:600)).^2);
% mse7 = mean((prediction_hid(1,601:800)-hid_test2(1,601:800)).^2);
% mse8 = mean((prediction_hid(2,601:800)-hid_test2(2,601:800)).^2);
% mse9 = mean((prediction_hid(1,801:1000)-hid_test2(1,801:1000)).^2);
% mse10 = mean((prediction_hid(2,801:1000)-hid_test2(2,801:1000)).^2);
% mse11 = mean((prediction_hid(1,1001:1400)-hid_test2(1,1001:1400)).^2);
% mse12 = mean((prediction_hid(2,1001:1400)-hid_test2(2,1001:1400)).^2);
% 
% figure()
% text(100,100,'6d ')
% subplot(2,3,1)
% plot(prediction_hid(1,1:200),prediction_hid(2,1:200),'*r',hid_test2(1,1:200),hid_test2(2,1:200),'*g');
% d1 = sprintf('1:200 - mse = (%f - %f)',mse1,mse2)
% title(d1)
% subplot(2,3,2)
% plot(prediction_hid(1,201:400),prediction_hid(2,201:400),'*r',hid_test2(1,201:400),hid_test2(2,201:400),'*g');
% d1 = sprintf('201:400 - mse = (%f - %f)',mse3,mse4)
% title(d1)
% subplot(2,3,3)
% plot(prediction_hid(1,401:600),prediction_hid(2,401:600),'*r',hid_test2(1,401:600),hid_test2(2,401:600),'*g');
% d1 = sprintf('401:600 - mse = (%f - %f)',mse5,mse6)
% title(d1)
% subplot(2,3,4)
% plot(prediction_hid(1,601:800),prediction_hid(2,601:800),'*r',hid_test2(1,601:800),hid_test2(2,601:800),'*g');
% d1 = sprintf('601:800 - mse = (%f - %f)',mse7,mse8)
% title(d1)
% subplot(2,3,5)
% plot(prediction_hid(1,801:1000),prediction_hid(2,801:1000),'*r',hid_test2(1,801:1000),hid_test2(2,801:1000),'*g');
% d1 = sprintf('801:1000 - mse = (%f - %f)',mse9,mse10)
% title(d1)
% subplot(2,3,6)
% plot(prediction_hid(1,1001:1400),prediction_hid(2,1001:1400),'*r',hid_test2(1,1001:1400),hid_test2(2,1001:1400),'*g');
% d1 = sprintf('1001:1400 - mse = (%f - %f)',mse11,mse12)
% title(d1)
%%
% figure()
% text(100,100,'6d ')
% subplot(2,2,1)
% plot(prediction_hid(1,1:125),prediction_hid(2,1:125),'*r',hid_test2(1,1:125),hid_test2(2,1:125),'*g');
% d1 = sprintf('1:125 - mse = (%f - %f)',mse1,mse2)
% title(d1)
% subplot(2,2,2)
% plot(prediction_hid(1,126:250),prediction_hid(2,126:250),'*r',hid_test2(1,126:250),hid_test2(2,126:250),'*g');
% d1 = sprintf('126:250 - mse = (%f - %f)',mse3,mse4)
% title(d1)
% subplot(2,2,3)
% plot(prediction_hid(1,251:375),prediction_hid(2,251:375),'*r',hid_test2(1,251:375),hid_test2(2,251:375),'*g');
% d1 = sprintf('251:375 - mse = (%f - %f)',mse5,mse6)
% title(d1)
% subplot(2,2,4)
% plot(prediction_hid(1,376:499),prediction_hid(2,376:499),'*r',hid_test2(1,376:499),hid_test2(2,376:499),'*g');
% d1 = sprintf('376:499 - mse = (%f - %f)',mse7,mse8)
% title(d1)


%%



% figure()
% plot(linspace(1,length(prediction),length(prediction)),prediction(1,:),'*r',linspace(1,1402,1402),obs_test(1,1:1402),'*g');
% 
% figure()
% plot(linspace(1,length(prediction_hid),length(prediction_hid)),prediction_hid(1,:),'*r',linspace(1,1402,1402),hid_test2(1,1:1402),'*g');
% 
% 
% figure()
% plot(linspace(1,length(prediction_hid),length(prediction_hid)),prediction_hid(2,:),'*r',linspace(1,1402,1402),hid_test2(2,1:1402),'*g');

%%
figure()
plot(linspace(1,1118,1118),hid_test(1,:),'r*',linspace(1,1118,1118),prediction_hid(1,:),'g*')
title('smoothing-features: angle and velocity- angle')
figure()
plot(linspace(1,1118,1118),hid_test(2,:),'r*',linspace(1,1118,1118),prediction_hid(2,:),'g*')
title('smoothing-features: angle and velocity- velocity')
%%
figure()
subplot(2,3,1)
plot(linspace(1,154,154),hid_test(1,1:154),'r*',linspace(1,154,154),prediction_hid(1,1:154),'g*');
title('test chunk 1');
subplot(2,3,2)
plot(linspace(1,267,267),hid_test(1,155:421),'r*',linspace(1,267,267),prediction_hid(1,155:421),'g*');
title('test chunk 2');
subplot(2,3,3)
plot(linspace(1,265,265),hid_test(1,422:686),'r*',linspace(1,265,265),prediction_hid(1,422:686),'g*');
title('test chunk 3');
subplot(2,3,4)
plot(linspace(1,151,151),hid_test(1,687:837),'r*',linspace(1,151,151),prediction_hid(1,687:837),'g*');
title('test chunk 4');
subplot(2,3,5)
plot(linspace(1,281,281),hid_test(1,838:1118),'r*',linspace(1,281,281),prediction_hid(1,838:1118),'g*');
title('test chunk 5');

%%

figure()
subplot(2,3,1)
plot(linspace(1,154,154),hid_test(2,1:154),'r*',linspace(1,154,154),prediction_hid(2,1:154),'g*');
title('test chunk 1');
subplot(2,3,2)
plot(linspace(1,267,267),hid_test(2,155:421),'r*',linspace(1,267,267),prediction_hid(2,155:421),'g*');
title('test chunk 2');
subplot(2,3,3)
plot(linspace(1,265,265),hid_test(2,422:686),'r*',linspace(1,265,265),prediction_hid(2,422:686),'g*');
title('test chunk 3');
subplot(2,3,4)
plot(linspace(1,151,151),hid_test(2,687:837),'r*',linspace(1,151,151),prediction_hid(2,687:837),'g*');
title('test chunk 4');
subplot(2,3,5)
plot(linspace(1,281,281),hid_test(2,838:1118),'r*',linspace(1,281,281),prediction_hid(2,838:1118),'g*');
title('test chunk 5');