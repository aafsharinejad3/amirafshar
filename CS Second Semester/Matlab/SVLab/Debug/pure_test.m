% 
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Initializations
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear all;
clc;
% 
addpath('\Users\lab\Desktop\CS First Semester\Matlab\SVLab\svlab');
% Path to svlab
clipStart = 0;
clipEnd = 0;
trainingData = 2500;
hankelSize = 250;
simExtent = 5000;
lowD = 500;

% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Load Data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading Data');
pause(.1);
load -ASCII 110909_2
Y = X110909_2(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;

Ytrain = Y(1:floor(0.8*8403));
Ytest = Y(floor(0.8*8403)+1:end);
[d,M] = size(Ytrain);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build Hankel Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Building Hankel matrix');
pause(.1);
tic
tau = M-hankelSize+1;    
X = zeros(hankelSize*d, tau);
for i = 1:hankelSize
       X((i-1)*d + 1:i*d,1:tau) = Ytrain(:,(1:tau)-1 + i);
end
M = tau;
toc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute HSE-PSR Paramters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Learning Model');
pause(.1);
tic
futures      = X(:, 1:M-1);  % Immediate future sequences of observations 
observations = X(1:d, 1:M-1);% Current observation
extfutures   = X(:,2:M);    % Future one-step-removed sequences of observations
%%
% Find kernel bandwidth as the median of the pairwise distances (the median
% trick).

% % Bandwidth for sequences of obs
disx = fastDist(X(:,1:M),X(:,1:M));
disx = disx(:);
mdisx = median(disx);

% Gaussian RBF Kernel (for observation sequences: histories & futures)
kernelx = rbf_dot(1./mdisx);

% Bandwidth for current obs
disx2 = fastDist(observations(:,1:M-1),observations(:,1:M-1));
disx2 = disx2(:);
mdisx2 = median(disx2);

% Gaussian RBF Kernel (for current observations)
kernelx2 = rbf_dot(1./mdisx2);
%%

startFrame = 1000;
K = sv_dot(kernelx2,observations);
K_x = sv_dot(kernelx2,observations,Ytest);

alpha = K(:,startFrame-1);
alpha2 = alpha;
G = sv_dot(kernelx,futures);
G_tilda = sv_dot(kernelx,futures,extfutures);
I = eye(size(G,2));
summ = inv(G+1*I)*G_tilda;



%%

nextOb = [];
alpha_b = [];
 
nextOb_2 = [];
alpha_2 = [];
for i =1:40
    i
   alpha = (summ*diag(alpha))'*inv((diag(summ*alpha)*K)^2+1*I)*K*diag(summ*alpha)*K_x(:,i);
   alpha = alpha./sum(alpha);
   alpha_b = [alpha_b,alpha];
   nextOb = [nextOb,observations*alpha];
%    
%    alpha2 = diag(summ*alpha2)*K*inv((diag(summ*alpha2)*K)^2+1*I)*diag(summ*alpha2)*K_x(:,i);
%     alpha2 = alpha2./sum(alpha2);
%    alpha_2 = [alpha_2,alpha2];
%     nextOb_2 = [nextOb_2,observations*alpha2];
    
end


%%

figure()
plot(linspace(1,27,27),nextOb,'*r',linspace(1,40,40),Ytest(1:40),'*g');
ylim([0 1])