%% In this m file, first we find alpha and alpha_tilda s.t. 
% Phi*diag(alpha)*Phi = Phi_tilda*diag(alpha_tilda)*Phi_tilda
% Then we check to see wether we can get alpha from the following equation
% or not:
% alpha = inv(G+lambda*I)*G_tilda*alpha_tilda

close all
clear all;
clc;

% Path to svlab
addpath('../svlab');

clipStart = 0;
clipEnd = 0;
trainingData = 2500;
hankelSize = 250;
simExtent = 5000;
lowD = 500;

load('alpha_b_2.mat');
alpha_b_2 = alpha_b;

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Load Data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading Data');
pause(.1);
tic
load './windfarm_medium.mat';
Y = images(:,clipStart+1:end-clipEnd);
[Uy,Sy,Vy] = svd(Y(:,1:trainingData),'econ');

Uy = Uy(:,1:lowD);
Sy = Sy(1:lowD,1:lowD);
Vy = Vy(:,1:lowD);

Ytrain = Sy*Vy';
clear Y;
[d,M] = size(Ytrain);
 toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build Hankel Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Building Hankel matrix');
pause(.1);
tic
tau = M-hankelSize+1;    
X = zeros(hankelSize*d, tau);
for i = 1:hankelSize
       X((i-1)*d + 1:i*d,1:tau) = Ytrain(:,(1:tau)-1 + i);
end
M = tau;
toc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute HSE-PSR Paramters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Learning Model');
pause(.1);
tic
futures      = X(:, 1:M-1);  % Immediate future sequences of observations 
observations = X(1:d, 1:M-1);% Current observation
extfutures   = X(:,2:M);    % Future one-step-removed sequences of observations
toc

%% Kernel Defining
% futures is y_i (which is used to obtain Phi) and extfuture is  y_tilda_i
% (which is used to obtain Phi_tilda)

% Find kernel bandwidth as the median of the pairwise distances (the median
% trick).
disp('Kernels');
% % Bandwidth for sequences of obs
tic
disx = fastDist(X(:,1:M),X(:,1:M));
disx = disx(:);
mdisx = median(disx);

% Gaussian RBF Kernel (for observation sequences: histories & futures)
kernelx = rbf_dot(1./mdisx);

% Bandwidth for current obs
disx2 = fastDist(observations(:,1:M-1),observations(:,1:M-1));
disx2 = disx2(:);
mdisx2 = median(disx2);

% Gaussian RBF Kernel (for current observations)
kernelx2 = rbf_dot(1./mdisx2);
toc
%% Finding alpha and alpha_tilda
G = sv_dot(kernelx , futures);
G_tilda = sv_dot(kernelx , futures ,extfutures);

%% One example of alpha and alpha_tilda which works
alpha = zeros(2250,1);
alpha_tilda = zeros(2250,1);
alpha(1:15) = [0,0.05,0.1,0.2,0.15,0.03,0.07,0.06,0.04,0.02,0.08,0.05,0.03,0.04,0.08];
alpha_tilda(1:14) = [0.05,0.1,0.2,0.15,0.03,0.07,0.06,0.04,0.02,0.08,0.05,0.03,0.04,0.08];

comp1 = G*diag(alpha)*G;
comp2 = G_tilda*diag(alpha_tilda)*G_tilda';

%% Test
I = eye(2250);
lambda = 0.1;
alpha_test = inv(G+lambda*I)*G_tilda*alpha_tilda;
figure()
plot(alpha);
figure()
plot(alpha_test);
figure()
plot(alpha-alpha_test);

%% Second example of alpha and alpha_tilda which works

alpha2 = zeros(2250,1);
alpha2_tilda = zeros(2250,1);
alpha2(2:end) = alpha_b_2(1:end-1,10);
alpha2_tilda(1:end-1) = alpha_b_2(1:end-1,10);
% alpha2_tilda
comp1_2 = G*diag(alpha2)*G;
comp2_2 = G_tilda*diag(alpha2_tilda)*G_tilda';


%% Test
lambda2 = 0.1;
alpha_test2 = inv(G+lambda2*I)*G_tilda*alpha2_tilda;

figure()
plot(alpha2);
figure()
plot(alpha_test2);
figure()
plot(alpha2-alpha_test2);