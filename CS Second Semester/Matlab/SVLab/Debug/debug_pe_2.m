close all
clear all;
clc;
% 
% Path to svlab
addpath('svlab');

clipStart = 0;
clipEnd = 0;
trainingData = 2500;
hankelSize = 250;
simExtent = 5000;
lowD = 500;

load('alpha_b_2.mat');
alpha_b_2 = alpha_b;
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Load Data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading Data');
pause(.1);
load 'windfarm_medium.mat'
Y = images(:,clipStart+1:end-clipEnd);
[Uy,Sy,Vy] = svd(Y(:,1:trainingData),'econ');

Uy = Uy(:,1:lowD);
Sy = Sy(1:lowD,1:lowD);
Vy = Vy(:,1:lowD);
%%
Ytrain = Sy*Vy';
clear Y;
[d,M] = size(Ytrain);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build Hankel Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Building Hankel matrix');
pause(.1);
tic
tau = M-hankelSize+1;    
X = zeros(hankelSize*d, tau);
for i = 1:hankelSize
       X((i-1)*d + 1:i*d,1:tau) = Ytrain(:,(1:tau)-1 + i);
end
M = tau;
toc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute HSE-PSR Paramters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Learning Model');
pause(.1);
tic
futures      = X(:, 1:M-1);  % Immediate future sequences of observations 
observations = X(1:d, 1:M-1);% Current observation
extfutures   = X(:,2:M);    % Future one-step-removed sequences of observations

% Bandwidth for current obs
disx = fastDist(X(:,1:M),X(:,1:M));
disx = disx(:);
mdisx = median(disx);

% Gaussian RBF Kernel (for current observations)
kernelx = rbf_dot(1./mdisx);
%%


y_test = -1000 + (2000).*rand(500,1000);
[d1,M1] = size(y_test);

tau1 = M1-hankelSize+1;    
h_test = zeros(hankelSize*d1, tau1);
for i = 1:hankelSize
       h_test((i-1)*d1 + 1:i*d1,1:tau1) = y_test(:,(1:tau1)-1 + i);
end
M1 = tau1;


k_y = sv_dot(kernelx,futures,h_test);
k_y_tilda = sv_dot(kernelx,extfutures,h_test);

%%
w = ones(1,size(alpha_b_2,1))./size(alpha_b_2,1);
g1 = zeros(1,size(h_test,2));
g2 = zeros(1,size(h_test,2));
for i = 1:size(alpha_b_2,1)
    g1 = g1 + w(i)*k_y(i,:);
    g2 = g2 + alpha_b_2(i,15)*k_y_tilda(i,:);
end

%%

figure()

plot(1:751,g1,'r*',1:751,g2,'bo');
legend('stationary distribution', 'conditional embedding');

figure()

plot(1:751,g2,'r*');



figure
subplot(1,2,1)
hist(g1)
subplot(1,2,2)
hist(g2)