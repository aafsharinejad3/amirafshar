
clear all;
clc;
% 
% Path to svlab
addpath('../svlab');

clipStart = 0;
clipEnd = 0;
trainingData = 1000;
hankelSize = 250;
simExtent = 5000;
lowD = 500;

load('alpha_b.mat');

% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Load Data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading Data');
pause(.1);
load -ASCII 110909_2
Y = X110909_2(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;

Ytrain = Y(1:floor(0.8*8403));
Ytest = Y(floor(0.8*8403)+1:end);
[d,M] = size(Ytrain);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build Hankel Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Building Hankel matrix');
pause(.1);
tic
tau = M-hankelSize+1;    
X = zeros(hankelSize*d, tau);
for i = 1:hankelSize
       X((i-1)*d + 1:i*d,1:tau) = Ytrain(:,(1:tau)-1 + i);
end
M = tau;
toc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute HSE-PSR Paramters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Learning Model');
pause(.1);
tic
futures      = X(:, 1:M-1);  % Immediate future sequences of observations 
observations = X(1:d, 1:M-1);% Current observation
extfutures   = X(:,2:M);    % Future one-step-removed sequences of observations

% Bandwidth for current obs
disx = fastDist(X(:,1:M),X(:,1:M));
disx = disx(:);
mdisx = median(disx);

% Gaussian RBF Kernel (for current observations)
kernelx = rbf_dot(1./mdisx);

% Bandwidth for current obs
disx2 = fastDist(observations(:,1:M-1),observations(:,1:M-1));
disx2 = disx2(:);
mdisx2 = median(disx2);

% Gaussian RBF Kernel (for current observations)
kernelx2 = rbf_dot(1./mdisx2);

%%

y_test = 0:1/1248:1;
[d1,M1] = size(y_test);

tau1 = M1-hankelSize+1;    

h_test = zeros(hankelSize*d1, tau1);
for i = 1:hankelSize
       h_test((i-1)*d1 + 1:i*d1,1:tau1) = Ytest(:,(1:tau1)-1 + i);
end
M1 = tau1;
K = sv_dot(kernelx2,observations);
K_x = sv_dot(kernelx2,observations,0.8);
G = sv_dot(kernelx,futures);
G_tilda = sv_dot(kernelx,futures,extfutures);
G_y = sv_dot(kernelx,futures,h_test);
G_y_tilda = sv_dot(kernelx,extfutures,h_test);
G_tt = sv_dot(kernelx,extfutures);
I = eye(6472);
%%
alpha = (1/6472).*ones(6472,1);
lambda = 0.1;

% est_a = G_tt*diag(alpha)*G_y_tilda;
% 
% est_b = G_tt*diag(inv(G_tt+1*I)*G_tt*alpha)*G_y_tilda;
% 
% est_c = G*diag(inv(G+1*I)*G_tilda*alpha)*G_y;

est_a = inv(G+lambda*I)*G_tilda*diag(alpha);

est_b = diag(inv(G_tt+lambda*I)*G_tt*alpha);

est_c = diag(inv(G+lambda*I)*G_tilda*alpha);

% D = diag(inv(G+lambda*I)*G_tilda*alpha);
% D_id = diag(inv(G_tt+lambda*I)*G_tt*alpha);
% 
% est_a = (inv(G+10*I)*G_tilda*diag(alpha))'*inv((D*K)^2+lambda*I)*K*D*K_x;
% 
% est_b = D_id*K*inv((D_id*K)^2+lambda*I)*D_id*K_x;
% 
% est_c = D*K*inv((D*K)^2+lambda*I)*D*K_x;


figure()
subplot(3,1,1)
imagesc(est_a)
colorbar
title('Estimation (a)-Not working');


subplot(3,1,2)
imagesc(est_b)
colorbar
title('Estimation (b)-Ideal');

subplot(3,1,3)
imagesc(est_c)
colorbar
title('Estimation (c)-Working');

%%
isequal(est_a,est_b);