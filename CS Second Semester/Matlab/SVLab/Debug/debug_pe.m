
clear all;
clc;
% 
% Path to svlab
addpath('\Users\lab\Desktop\Cs First Semester\Matlab\SVLab\svlab');

clipStart = 0;
clipEnd = 0;
trainingData = 1000;
hankelSize = 250;
simExtent = 5000;
lowD = 500;

load('alpha_b.mat');

% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Load Data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Loading Data');
pause(.1);
load -ASCII 110909_2
Y = X110909_2(:,11)';
Y = cos(Y./max(Y)*2*pi)./2 + .5;

Ytrain = Y(1:floor(0.8*8403));
Ytest = Y(floor(0.8*8403)+1:end);
[d,M] = size(Ytrain);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build Hankel Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Building Hankel matrix');
pause(.1);
tic
tau = M-hankelSize+1;    
X = zeros(hankelSize*d, tau);
for i = 1:hankelSize
       X((i-1)*d + 1:i*d,1:tau) = Ytrain(:,(1:tau)-1 + i);
end
M = tau;
toc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute HSE-PSR Paramters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Learning Model');
pause(.1);
tic
futures      = X(:, 1:M-1);  % Immediate future sequences of observations 
observations = X(1:d, 1:M-1);% Current observation
extfutures   = X(:,2:M);    % Future one-step-removed sequences of observations

% Bandwidth for current obs
disx = fastDist(X(:,1:M),X(:,1:M));
disx = disx(:);
mdisx = median(disx);

% Gaussian RBF Kernel (for current observations)
kernelx = rbf_dot(1./mdisx);

%%

y_test = 0:1/1248:1;
[d1,M1] = size(y_test);

tau1 = M1-hankelSize+1;    
h_test = zeros(hankelSize*d1, tau1);
for i = 1:hankelSize
       h_test((i-1)*d1 + 1:i*d1,1:tau1) = Ytest(:,(1:tau1)-1 + i);
end
M1 = tau1;


k_y = sv_dot(kernelx,futures,h_test);
k_y_tilda = sv_dot(kernelx,extfutures,h_test);
%%
w = ones(1,size(alpha_b,1))./size(alpha_b,1);
g1 = zeros(1,size(h_test,2));
g2 = zeros(1,size(h_test,2));
for i = 1:size(alpha_b,1)
    g1 = g1 + w(i)*k_y(i,:);
    g2 = g2 + alpha_b(i,15)*k_y_tilda(i,:);
end

%%
figure()

plot(1:1000,g1,'r*',1:1000,g2,'bo');
legend('stationary distribution', 'conditional embedding');

figure()

plot(1:1000,g1,'r*');


%%
figure
subplot(1,2,1)
hist(g1)
subplot(1,2,2)
hist(g2)