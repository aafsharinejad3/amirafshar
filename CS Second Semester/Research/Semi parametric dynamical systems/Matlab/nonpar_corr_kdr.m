%% Nonparametric Density Estimation with a Parametric Start

close all
Y_i=2*randn(1000,1)+2;
mu=[2 6];
sigma=[1 4];
obj=gmdistribution(mu,sigma);
X=random(obj,500);
figure()
hist(X)
title('mixture of the gaussian densities');
X_i=[X(:,1);X(:,2)];
figure()
hist(X_i)
title('density to be estimated')
%%
mu_hat=mean(X_i);
sigma2_hat=sum((X_i-mu_hat).^2)/length(X_i);
f_theta=@(x) exp(-(x-mu_hat).^2/(2*sigma2_hat))/sqrt(sigma2_hat*2*pi);
c = 1/sqrt(2*pi*sigma2_hat);
k_h=@(x,x1)1/length(X_i)*c*exp(-(x-x1).^2/(2*sigma2_hat));
x_s=linspace(-10,10,1000);
k=0;
for j=1:1000
k=k+k_h(X_i(j),x_s);
end
%%

f_hat2=0;
for i =1:length(X_i)
f_hat=@(x2) f_theta(x2).*k_h(X_i(i),x2)/(length(X_i)*f_theta(X_i(i)));
f_hat2=@(x2) f_hat2(x2)+f_hat(x2);
end



f=@(x3)exp(-x3.^2/8)/sqrt(2*pi*4);


%%


zo=f(x_s);
z=f_hat(x_s);
n=max(zo)/max(z);
figure()
plot(x_s,n.*z,'r');
title('The gaussian estimation of the mixture of the gaussians')
figure()
hist(X_i);
