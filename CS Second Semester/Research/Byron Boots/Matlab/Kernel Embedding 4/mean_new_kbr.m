function [ a ] = mean_new_kbr( alpha , Y_p , X , Y , lambda , kernel1 , kernel2 , x )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[n,m]=size(X);
[B , L , L_x] = mean_kce(X , lambda , x , kernel2);
K = sv_dot(kernel1 , Y);
K_tilda = sv_dot(kernel1 , Y ,Y_p);

I = speye(m);
a = spdiags(alpha,0,length(alpha),length(alpha))*K_tilda'*(inv(K+lambda.*I))*(inv(L+lambda.*I))*L_x;
end

