% close all 
clear all
addpath('svlab')
l = 10000;
mu = [2 3];
sigma = [1 1.5;1.5 3];
R = chol(sigma);
z = repmat(mu,l,1) + randn(l,2)*R;
z2 = randn(l,1);
%%
mu1 = [2 7];
sigma1 = [1 1.5;1.5 3];
R1 = chol(sigma1);
w = repmat(mu1,l,1) + randn(l,2)*R1;
w2 = randn(l,1);
%%
x_test = linspace(0,7,l)';
y_test = linspace(0,15,l)';
% x_test = -1 + (16).*rand(l,1);
% y_test = -1 + (16).*rand(l,1);
% [xoo,yoo]= meshgrid(0:7/39:7,0:15/39:15);
% x_test = xoo(:);
% y_test = yoo(:);
% 
% x_test = x;
% y_test = y;
%%
% close all
% figure()
% scatter(z(:,1),z(:,2));


x = z(:,1);
y = z(:,2);

x_tilda_d = w(:,1);
y_tilda_d = w(:,2);
x_n = (x-mean(x))./std(x);
% y_tilda = (y-3)./sqrt(3)*3+7;
y_tilda = sqrt(3).*((sqrt(3)/2).*x_n + 0.5.*z2) + 10;
% y_tilda_c = (y-mean(y))./std(y);
% y_tilda = sqrt(3)*y_tilda_c + 7;
% figure()
% scatter(x,y_tilda);

disx = fastDist(x',x');
disx = disx(:);
mdisx = median(disx);

disx2 = fastDist(y',y');
disx2 = disx2(:);
mdisx2 = median(disx2);

disx3 = fastDist(y',y_tilda');
disx3 = disx3(:);
mdisx3 = median(disx3);

disx4 = fastDist(y_tilda',y_tilda');
disx4 = disx4(:);
mdisx4 = median(disx4);

kernelx = rbf_dot(1./mdisx);
kernely = rbf_dot(1./mdisx2);
kernelz = rbf_dot(1./mdisx3);
kernela = rbf_dot(1./mdisx4);

G = sv_dot(kernely,y');
G_tilda = sv_dot(kernelz,y',y_tilda');
alpha = (1/l).*ones(l,1);
K_x = sv_dot(kernelx,x_test',x');
G_y = sv_dot(kernely,y',y_test');
G_y_tilda = sv_dot(kernela,y_tilda',y_test');
I = eye(l);
%%
est_a = K_x*inv(G+10*I)*G_tilda*diag(alpha)*G_y_tilda;

est_b = K_x*diag(inv(G+10*I)*G_tilda*alpha)*G_y;


disx5 = fastDist(y_tilda_d',y_tilda_d');
disx5 = disx5(:);
mdisx5 = median(disx5);
kernelb = rbf_dot(1./mdisx5);

disx5 = fastDist(y_tilda_d',y_tilda_d');
disx5 = disx5(:);
mdisx5 = median(disx5);
kernelb = rbf_dot(1./mdisx5);

G_d = sv_dot(kernelb,y_tilda_d');
G_tilda_d = G_d;
K_x_d = sv_dot(kernelx,x_test',x_tilda_d');
G_y_tilda_d = sv_dot(kernelb,y_tilda_d',y_test');

est_c = K_x_d*inv(G_d+I)*G_tilda_d*diag(alpha)*G_y_tilda_d;

est_d = K_x_d*diag(inv(G_d+I)*G_tilda_d*alpha)*G_y_tilda_d;

figure()
subplot(2,2,1)
imagesc(y_test, x_test, est_a)
colorbar
title('Estimation (a)');

subplot(2,2,2)
imagesc(y_test, x_test,est_b)
colorbar
title('Estimation (b)');

subplot(2,2,3)
imagesc(y_test, x_test,est_c)
colorbar
title('Estimation (a)-direct');

subplot(2,2,4)
imagesc(y_test, x_test,est_d)
colorbar
title('Estimation (b)-direct');