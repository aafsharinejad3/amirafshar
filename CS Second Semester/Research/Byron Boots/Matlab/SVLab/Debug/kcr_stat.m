clear all 
close all

addpath('svlab');
l = 160;
l2 = 100;

y = rand(l,1);
y_tilda = rand(l,1);
x = rand(l2,1);

x = x./sum(x);

y = y./sum(y);

y_tilda = y_tilda./sum(y_tilda);
% y_tilda = round(y_c_tilda.*100000)./100000;

% p_x_y = rand(l,l2);
% s = sum(p_x_y,2);
% p_x_y = p_x_y./repmat(s,1,l2);

% p_xy = repmat(y',1,l2).*p_x_y;
% p_xytilda = repmat(y_tilda',1,l2).*p_x_y;
p_xy = x*y';
p_xytilda = x*y_tilda';
p_x = sum(p_xy,2);
p_xx = sum(p_xytilda,2);

c_x_y = repmat(p_x,1,l);
c_yx = p_xytilda';
c_xy = p_xytilda;
c_xx = diag(p_x);
c_yy = diag(y_tilda);
c_yy_2 = diag(y);
c_xyy = zeros(l2,l,l);

for i = 1:l
    for j = 1:l
        if i == j
            c_xyy(:,i,i) = p_xytilda(:,i);
        end
    end
end

c_xyy_2 = zeros(l2,l,l);

for i = 1:l
    for j = 1:l
        if i == j
            c_xyy_2(:,i,i) = p_xy(:,i);
        end
    end
end

for i = 1:l
c_xy_y_2(:,:,i) = c_xyy_2(:,:,i)*inv(c_yy_2);
end

for i = 1:l
c_xy_y(:,:,i) = c_xyy(:,:,i)*inv(c_yy);
end

for i =1:l
    c_xy_pi(:,i) = c_xy_y(:,:,i)*y_tilda;
end


for i =1:l
    c_xy_pi_3(:,i) = c_xy_y_2(:,:,i)*y_tilda;
end
c_xy_pi_2 = c_x_y*c_yy;
% mu = [2 3];
% sigma = [1 1.5;1.5 3];
% R = chol(sigma);
% z = repmat(mu,l,1) + randn(l,2)*R;
% z2 = randn(l,1);
% x = z(:,1);
% y = z(:,2);
% x_n = (x-mean(x))./std(x);
% y_tilda = sqrt(3).*((sqrt(3)/2).*x_n + 0.5.*z2) + 7;
% %%

%  figure()
%  plot(y);
%  o = mvnpdf(z,mu,sigma);
%  figure()
%  plot(o)