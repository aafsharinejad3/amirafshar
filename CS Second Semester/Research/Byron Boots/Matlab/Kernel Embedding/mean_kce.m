function [ B ] = mean_kce( X , Y , lambda , x )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
[n,m] = size(X);
disx = pdist(X').^2;
mdisx = median(disx);
kernel = rbf_dot(1./(1e+1*mdisx));


K = sv_dot(kernel,X);
I = eye(m);
a = inv(K+lambda*I);

K_x = sv_dot(kernel,x,X)';
B = a*K_x;

end

