function [ a ] = mean_ksr( alpha , Y_p , Y , lambda , kernel)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
[n,m] = size(Y_p);
[p,q] = size(Y);
[l,o] = size(alpha);
if (l ~= 1 & o ~= 1)
    disp('Alpha is a vector of length "m" which is the number of data points, please try again')
    return
elseif (l ~= m & o ~= m)
    disp('The length of the vector is not correctly set, it must be the same as the number of data points "m", please try again')
    return
end
if (p ~= n)
    disp('The number of dimensions for prior ys "Y_p" and "Y" must be equal, please try again')
    return
end
if (q ~= m)
    disp('The number of data points for prior ys "Y_p" and "Y" must be equal, please try again')
    return
end

G = sv_dot(kernel,Y);
G_tilda = sv_dot(kernel,Y,Y_p);
I = eye(m);
c = inv(G+lambda*I);
a = c*G_tilda*alpha;
end

