function [ a ] = cov_kce( X , Y , lambda )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
[n,m] = size(X);
disx = pdist(X').^2;
mdisx = median(disx);
kernel = rbf_dot(1./(1e+1*mdisx));

K = sv_dot(kernel,X);
I = eye(m);
a = inv(K+lambda*I);
end

