close all
load usps_resampled

faceW = 16; 
faceH = 16; 
numPerLine = 16; 
ShowLine = 8; 
Y = zeros(faceH*ShowLine,faceW*numPerLine); 
for i=0:ShowLine-1 
  	for j=0:numPerLine-1 
    	 Y(i*faceH+1:(i+1)*faceH,j*faceW+1:(j+1)*faceW) = reshape(train_patterns(:,i*numPerLine+j+1),[faceH,faceW]); 
  	end 
end 
figure()
imagesc(Y');
colormap(gray);
axis off;
axis equal;

%%
 idx=find (train_labels(3,:)==1);
 o=train_patterns(:,idx);
 x=o(:,[1:109,111:296,298:475]);
 
 
 %%
 
 m = size(x, 2); 

G = sqrt(sum(x.^2,1)'*ones(1,m) + ones(m,1)*sum(x.^2,1) - 2*(x'*x));
 e = 0.8*median(G(:));  
 G(G>e) = 0; 
sG = sum(G, 1); 

% get rid of Inf distance for simplicity; 
i = find(sG == 0); 
idx = setdiff((1:m), i); 
G = G(idx,idx); 
m = size(G, 1); 

spy(sparse(G)); 
drawnow; 

D = graphallshortestpaths(sparse(G), 'directed', 'false');
zooo=find(D>10^20);
%  D(D>10^20)=0;
 
D2 = D.^2; 

H = eye(m) - ones(m,1)*ones(m,1)'./m; 

Dt = -0.5 * H * D2 * H; 

k = 10; 
[V, S, U] = svds(Dt, k);

dim1 = V(:,1) * sqrt(S(1,1)); 
dim2 = V(:,2) * sqrt(S(2,2)); 

%%

figure()
% imshow(Y');
%  colormap(gray);
% % % axis off;
% % % axis equal;
%  hold on
scatter(dim1, dim2,18*ones(1,m),'fill');

%%

th = 0:pi/50:2*pi;
a=find(dim1>13.18 & dim1<13.2);
xunit_a = 0.5 * cos(th) + 13.19;
yunit_a = 0.5 * sin(th) + 8.177;

b=find(dim1>-10.02 & dim1<-10);
xunit_b = 0.5 * cos(th) + -10.01;
yunit_b = 0.5 * sin(th) + -5.913;

c=find(dim1>7.485 & dim1<7.487);
xunit_c = 0.5 * cos(th) + 7.486;
yunit_c = 0.5 * sin(th) + -7.533;

d=find(dim1>-10.34 & dim1<-10.32);
xunit_d = 0.5 * cos(th) + -10.33;
yunit_d = 0.5 * sin(th) + 5.519;

e=find(dim1>-5.193 & dim1<-5.191);
xunit_e = 0.5 * cos(th) + -5.192;
yunit_e = 0.5 * sin(th) + 5.242;

f=find(dim1>-16.67 & dim1<-16.65);
xunit_f = 0.5 * cos(th) + -16.66;
yunit_f = 0.5 * sin(th) + 4.828;

g=find(dim1>-10.05 & dim1<-10.03);
xunit_g = 0.5 * cos(th) + -10.04;
yunit_g = 0.5 * sin(th) + 0.2263;

h=find(dim1>-1.16 & dim1<-1.14);
xunit_h = 0.5 * cos(th) + -1.15;
yunit_h = 0.5 * sin(th) + 5.008;

i=find(dim1>3.74 & dim1<3.75);
xunit_i = 0.5 * cos(th) + 3.745;
yunit_i = 0.5 * sin(th) + 5.016;

j=find(dim1>8.25 & dim1<8.26);
xunit_j = 0.5 * cos(th) + 8.256;
yunit_j = 0.5 * sin(th) + 5.347;

k=find(dim1>6.09 & dim1<6.1);
xunit_k = 0.5 * cos(th) + 6.095;
yunit_k = 0.5 * sin(th) + 9.156;

l=find(dim1>1.03 & dim1<1.035);
xunit_l = 0.5 * cos(th) + 1.032;
yunit_l = 0.5 * sin(th) + 9.053;

n=find(dim1>-2.438 & dim1<-2.436);
xunit_n = 0.5 * cos(th) + -2.437;
yunit_n = 0.5 * sin(th) + 9.062;

o=find(dim1>6.585 & dim1<6.59);
xunit_o = 0.5 * cos(th) + 6.589;
yunit_o = 0.5 * sin(th) + 11.77;

p=find(dim1>-5.175 & dim1<-5.17);
xunit_p = 0.5 * cos(th) + -5.171;
yunit_p = 0.5 * sin(th) + 0.39;

q=find(dim1>-0.6052 & dim1<-0.605);
xunit_q = 0.5 * cos(th) + -0.6051;
yunit_q = 0.5 * sin(th) + 0.1347;

r=find(dim1>4.46 & dim1<4.47);
xunit_r = 0.5 * cos(th) + 4.465;
yunit_r = 0.5 * sin(th) + -0.002556;

s=find(dim1>9.07 & dim1<9.09);
xunit_s = 0.5 * cos(th) + 9.08;
yunit_s = 0.5 * sin(th) + 0.5817;

t=find(dim1>-6.015 & dim1<-6.01);
xunit_t = 0.5 * cos(th) + -6.013;
yunit_t = 0.5 * sin(th) + -5.145;

u=find(dim1>-1.06 & dim1<-1.058);
xunit_u = 0.5 * cos(th) + -1.059;
yunit_u = 0.5 * sin(th) + -4.821;

v=find(dim1>3.94 & dim1<3.942);
xunit_v = 0.5 * cos(th) + 3.941;
yunit_v = 0.5 * sin(th) + -4.683;

w=find(dim1>0.01 & dim1<0.015);
xunit_w = 0.5 * cos(th) + 0.01467;
yunit_w = 0.5 * sin(th) + -9.82;

z=find(dim1>-6.265 & dim1<-6.26);
xunit_z = 0.5 * cos(th) + -6.262;
yunit_z = 0.5 * sin(th) + -9.524;

z1=find(dim1>-15.05 & dim1<-14.95);
xunit_z1 = 0.5 * cos(th) + -15;
yunit_z1 = 0.5 * sin(th) + -4.335;

z2=find(dim1>10.87 & dim1<10.92);
xunit_z2 = 0.5 * cos(th) + 10.9;
yunit_z2 = 0.5 * sin(th) + -5.869;

z3=find(dim1>-13.75 & dim1<-13.7);
xunit_z3 = 0.5 * cos(th) + -13.72;
yunit_z3 = 0.5 * sin(th) + 1.335;

z4=find(dim1>11.55 & dim1<11.65);
xunit_z4 = 0.5 * cos(th) + 11.6;
yunit_z4 = 0.5 * sin(th) + 2.763;

z5=find(dim1>15.53 & dim1<15.55);
xunit_z5 = 0.5 * cos(th) + 15.54;
yunit_z5 = 0.5 * sin(th) + -11.3;
%%
a_img=reshape(x(:,135),[16 16]);
b_img=reshape(x(:,328),[16 16]);

c_img=reshape(x(:,410),[16 16]);
d_img=reshape(x(:,366),[16 16]);
e_img=reshape(x(:,171),[16 16]);
f_img=reshape(x(:,71),[16 16]);
g_img=reshape(x(:,129),[16 16]);
h_img=reshape(x(:,298),[16 16]);
i_img=reshape(x(:,327),[16 16]);
j_img=reshape(x(:,215),[16 16]);
k_img=reshape(x(:,294),[16 16]);

l_img=reshape(x(:,338),[16 16]);
n_img=reshape(x(:,210),[16 16]);
o_img=reshape(x(:,399),[16 16]);
p_img=reshape(x(:,227),[16 16]);
q_img=reshape(x(:,284),[16 16]);
r_img=reshape(x(:,176),[16 16]);
s_img=reshape(x(:,361),[16 16]);
t_img=reshape(x(:,288),[16 16]);
u_img=reshape(x(:,341),[16 16]);
v_img=reshape(x(:,222),[16 16]);
w_img=reshape(x(:,375),[16 16]);
z_img=reshape(x(:,110),[16 16]);
z1_img=reshape(x(:,257),[16 16]);
z2_img=reshape(x(:,14),[16 16]);
z3_img=reshape(x(:,85),[16 16]);
z4_img=reshape(x(:,382),[16 16]);
z5_img=reshape(x(:,152),[16 16]);
%%

close all
figure()
% imshow(Y');
%  colormap(gray);
% % % axis off;
% % % axis equal;
%  hold on
scatter(dim1, dim2,18*ones(473,1),'fill');
hold on
imagesc([12.19 14.19],[7.577 5.577],a_img');
colormap(gray)
plot(xunit_a, yunit_a,'red');

imagesc([-11.01 -9.01],[-6.513 -8.513],b_img');
colormap(gray)
plot(xunit_b, yunit_b,'red');

imagesc([6.486 8.486],[-8.133 -10.133],c_img');
colormap(gray)
plot(xunit_c, yunit_c,'red');

imagesc([-11.33 -9.33],[4.919 2.919],d_img');
colormap(gray)
plot(xunit_d, yunit_d,'red');

imagesc([-6.192 -4.192],[4.642 2.642],e_img');
colormap(gray)
plot(xunit_e, yunit_e,'red');

imagesc([-17.66 -15.66],[4.228 2.228],f_img');
colormap(gray)
plot(xunit_f, yunit_f,'red');

imagesc([-11.04 -9.04],[-0.5837 -2.5873],g_img');
colormap(gray)
plot(xunit_g, yunit_g,'red');

imagesc([-2.15 -0.15],[4.408 2.408],h_img');
colormap(gray)
plot(xunit_h, yunit_h,'red');

imagesc([2.745 4.745],[4.016 2.016],i_img');
colormap(gray)
plot(xunit_i, yunit_i,'red');

imagesc([7.256 9.256],[4.747 2.747],j_img');
colormap(gray)
plot(xunit_j, yunit_j,'red');


imagesc([5.095 7.095],[8.556 6.556],k_img');
colormap(gray)
plot(xunit_k, yunit_k,'red');

imagesc([0.032 2.032],[8.453 6.453],l_img');
colormap(gray)
plot(xunit_l, yunit_l,'red');

imagesc([-1.437 -3.437],[8.462 6.462],n_img');
colormap(gray)
plot(xunit_n, yunit_n,'red');

imagesc([7.189 9.189],[12.77 10.77],o_img');
colormap(gray)
plot(xunit_o, yunit_o,'red');

imagesc([-6.171 -4.171],[-0.21 -2.21],p_img');
colormap(gray)
plot(xunit_p, yunit_p,'red');

imagesc([-1.6051 0.3949],[-0.4653 -2.4653],q_img');
colormap(gray)
plot(xunit_q, yunit_q,'red');

imagesc([3.465 5.465],[-0.597444 -2.597444],r_img');
colormap(gray)
plot(xunit_r, yunit_r,'red');

imagesc([8.08 10.08],[-0.0183 -2.0183],s_img');
colormap(gray)
plot(xunit_s, yunit_s,'red');

imagesc([-7.013 -5.013],[-5.745 -7.745],t_img');
colormap(gray)
plot(xunit_t, yunit_t,'red');

imagesc([-2.059 -0.059],[-5.421 -7.421],u_img');
colormap(gray)
plot(xunit_u, yunit_u,'red');

imagesc([2.941 4.491],[-5.283 -7.283],v_img');
colormap(gray)
plot(xunit_v, yunit_v,'red');

imagesc([-0.98533 1.01467],[-10.42 -12.42],w_img');
colormap(gray)
plot(xunit_w, yunit_w,'red');

imagesc([-7.262 -5.262],[-10.124 -12.124],z_img');
colormap(gray)
plot(xunit_z, yunit_z,'red');

imagesc([-16 -14],[-4.995 -6.995],z1_img');
colormap(gray)
plot(xunit_z1, yunit_z1,'red');

imagesc([9.9 11.9],[-6.469 -8.469],z2_img');
colormap(gray)
plot(xunit_z2, yunit_z2,'red');

imagesc([-14.72 -12.72],[0.335 -1.665],z3_img');
colormap(gray)
plot(xunit_z3, yunit_z3,'red');

imagesc([10.6 12.6],[2.163 0.163],z4_img');
colormap(gray)
plot(xunit_z4, yunit_z4,'red');


imagesc([14.54 16.54],[-11.9 -13.9],z5_img');
colormap(gray)
plot(xunit_z5, yunit_z5,'red');