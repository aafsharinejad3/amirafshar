load 'face_data'
xx=double(images);

center=mean(xx,2);
 sz=size(xx,2);
 x = xx - center * ones(1, sz); 
 

%%
close all

faceW = 64; 
faceH = 64; 
numPerLine = 16; 
ShowLine = 8; 

Y = zeros(faceH*ShowLine,faceW*numPerLine); 
f=double(images);
for i=0:ShowLine-1 
  	for j=0:numPerLine-1 
    	 Y(i*faceH+1:(i+1)*faceH,j*faceW+1:(j+1)*faceW) = reshape(x(:,i*numPerLine+j+1),[faceH,faceW]); 
  	end 
end 
figure()
imagesc(Y);
colormap(gray);
axis off;
axis equal;

%%

covariance = cov(x'); 
% [U, S] = eig(covariance); 
[U, S] = eigs(covariance, 3); 

% proect x to the principal direction; 
Ux = U(:,1:2)' * x; 
%%
figure; 
scatter(Ux(1,:), Ux(2,:), 18*ones(1,sz),'filled'); 


%% Fitting images to plot

th = 0:pi/50:2*pi;
a=find(Ux(1,:)>-12.88 & Ux(1,:)<-12.86);
xunit_a = 0.5 * cos(th) + -12.87;
yunit_a = 0.5 * sin(th) + 7.131;

b=find(Ux(1,:)>-7.02 & Ux(1,:)<-7);
xunit_b = 0.5 * cos(th) + -7.01;
yunit_b = 0.5 * sin(th) + 7.961;

c=find(Ux(1,:)>-2.61 & Ux(1,:)<-2.60);
xunit_c = 0.5 * cos(th) + -2.608;
yunit_c = 0.5 * sin(th) + 9.03;

d=find(Ux(1,:)>2.21 & Ux(1,:)<2.22);
xunit_d = 0.5 * cos(th) + 2.216;
yunit_d = 0.5 * sin(th) + 7.823;

e=find(Ux(1,:)>6.687 & Ux(1,:)<6.689);
xunit_e = 0.5 * cos(th) + 6.688;
yunit_e = 0.5 * sin(th) + 8.025;

f=find(Ux(1,:)>-12.68 & Ux(1,:)<-12.66);
xunit_f = 0.5 * cos(th) + -12.67;
yunit_f = 0.5 * sin(th) + -0.3996;

g=find(Ux(1,:)>-7.85 & Ux(1,:)<-7.845);
xunit_g = 0.5 * cos(th) + -7.849;
yunit_g = 0.5 * sin(th) + -0.07263;


h=find(Ux(1,:)>-2.01 & Ux(1,:)<-2);
xunit_h = 0.5 * cos(th) + -2.001;
yunit_h = 0.5 * sin(th) + -0.1126;

i=find(Ux(1,:)>2.76 & Ux(1,:)<2.762);
xunit_i= 0.5 * cos(th) + 2.761;
yunit_i = 0.5 * sin(th) + 0.07668;

j=find(Ux(1,:)>7.537& Ux(1,:)<7.539);
xunit_j= 0.5 * cos(th) + 7.538;
yunit_j = 0.5 * sin(th) + 0.3146;

k=find(Ux(1,:)>-6.45 & Ux(1,:)<-6.44);
xunit_k= 0.5 * cos(th) + -6.446;
yunit_k = 0.5 * sin(th) + -8.875;

l=find(Ux(1,:)>-3.51 & Ux(1,:)<-3.49);
xunit_l= 0.5 * cos(th) + -3.5;
yunit_l = 0.5 * sin(th) + -6.264;

m=find(Ux(1,:)>1.103 & Ux(1,:)<1.105);
xunit_m= 0.5 * cos(th) + 1.104;
yunit_m = 0.5 * sin(th) + -7.177;

n=find(Ux(1,:)>4.281 & Ux(1,:)<4.283);
xunit_n= 0.5 * cos(th) + 4.282;
yunit_n = 0.5 * sin(th) + -7.097;

o=find(Ux(1,:)>7.84 & Ux(1,:)<7.842);
xunit_o= 0.5 * cos(th) + 7.841;
yunit_o = 0.5 * sin(th) + -6.649;

p=find(Ux(1,:)>-4.7 & Ux(1,:)<-4.68);
xunit_p= 0.5 * cos(th) + -4.69;
yunit_p = 0.5 * sin(th) + -15.14;

q=find(Ux(1,:)>-0.432 & Ux(1,:)<-0.431);
xunit_q= 0.5 * cos(th) + -0.4317;
yunit_q = 0.5 * sin(th) + -13.08;

r=find(Ux(1,:)>4.28 & Ux(1,:)<4.30);
xunit_r= 0.5 * cos(th) + 4.29;
yunit_r = 0.5 * sin(th) + -13.01;

s=find(Ux(1,:)>10.06 & Ux(1,:)<10.08);
xunit_s= 0.5 * cos(th) + 10.07;
yunit_s = 0.5 * sin(th) + 4.429;

t=find(Ux(1,:)>13.63 & Ux(1,:)<13.65);
xunit_t= 0.5 * cos(th) + 13.64;
yunit_t = 0.5 * sin(th) + 3.27;

z=find(Ux(1,:)>-10.56 & Ux(1,:)<-10.54);
xunit_z= 0.5 * cos(th) + -10.55;
yunit_z = 0.5 * sin(th) + 11.3;

z1=find(Ux(1,:)>-4.927 & Ux(1,:)<-4.925);
xunit_z1= 0.5 * cos(th) + -4.926;
yunit_z1 = 0.5 * sin(th) + 12.72;
%%

a_img=reshape(images(:,224),[64 64]);
b_img=reshape(images(:,71),[64 64]);
c_img=reshape(images(:,383),[64 64]);
d_img=reshape(images(:,338),[64 64]);
e_img=reshape(images(:,400),[64 64]);
f_img=reshape(images(:,166),[64 64]);
g_img=reshape(images(:,267),[64 64]);
h_img=reshape(images(:,215),[64 64]);
i_img=reshape(images(:,140),[64 64]);
j_img=reshape(images(:,56),[64 64]);
k_img=reshape(images(:,357),[64 64]);
l_img=reshape(images(:,157),[64 64]);
m_img=reshape(images(:,4),[64 64]);
n_img=reshape(images(:,271),[64 64]);
o_img=reshape(images(:,669),[64 64]);
p_img=reshape(images(:,490),[64 64]);
q_img=reshape(images(:,432),[64 64]);
r_img=reshape(images(:,449),[64 64]);
s_img=reshape(images(:,156),[64 64]);
t_img=reshape(images(:,502),[64 64]);
z_img=reshape(images(:,627),[64 64]);
z1_img=reshape(images(:,623),[64 64]);
%%

close all
figure()

scatter(Ux(1,:), Ux(2,:),18*ones(1,698),'fill');
title('PCA');
hold on
imagesc([-11.87 -13.87],[6.531 3.531],a_img);
colormap(gray)
plot(xunit_a, yunit_a,'red');

imagesc([-6.01 -8.01],[7.361 4.361],b_img);
colormap(gray)
plot(xunit_b, yunit_b,'red');


imagesc([-1.608 -3.608],[8.03 5.03],c_img);
colormap(gray)
plot(xunit_c, yunit_c,'red');


imagesc([1.216 3.216],[7.223 4.223],d_img);
colormap(gray)
plot(xunit_d, yunit_d,'red');

imagesc([5.668 7.668],[7.425 4.425],e_img);
colormap(gray)
plot(xunit_e, yunit_e,'red');

imagesc([-13.67 -11.67],[-0.9996 -3.9996],f_img);
colormap(gray)
plot(xunit_f, yunit_f,'red');

imagesc([-8.849  -6.849],[-0.67263 -3.67263],g_img);
colormap(gray)
plot(xunit_g, yunit_g,'red');

imagesc([1.761  3.761],[-0.52332 -3.52332],i_img);
colormap(gray)
plot(xunit_i, yunit_i,'red');

imagesc([-1.001  -3.001],[-0.7126 -3.7126],h_img);
colormap(gray)
plot(xunit_h, yunit_h,'red');

imagesc([6.538  8.538],[-0.2954 -3.2953],j_img);
colormap(gray)
plot(xunit_j, yunit_j,'red');

imagesc([-7.446 -5.446],[-9.475 -12.475],k_img);
colormap(gray)
plot(xunit_k, yunit_k,'red');

imagesc([-2.5 -4.5],[-6.864 -9.864],l_img);
colormap(gray)
plot(xunit_l, yunit_l,'red');

imagesc([0.104 2.104],[-7.777 -10.777],m_img);
colormap(gray)
plot(xunit_m, yunit_m,'red');

imagesc([3.282 5.282],[-7.697 -10.697],n_img);
colormap(gray)
plot(xunit_n, yunit_n,'red');

imagesc([6.841 8.841],[-7.249 -10.249],o_img);
colormap(gray)
plot(xunit_o, yunit_o,'red');

imagesc([-3.69 -5.69],[-15.74 -18.74],p_img);
colormap(gray)
plot(xunit_p, yunit_p,'red');

imagesc([-1.4317 0.5683],[-13.68 -16.68],q_img);
colormap(gray)
plot(xunit_q, yunit_q,'red');

imagesc([3.29 5.29],[-13.61 -16.61],r_img);
colormap(gray)
plot(xunit_r, yunit_r,'red');


imagesc([9.07 11.07],[3.629 0.629],s_img);
colormap(gray)
plot(xunit_s, yunit_s,'red');

imagesc([12.64 14.64],[2.67 -0.33],t_img);
colormap(gray)
plot(xunit_t, yunit_t,'red');

imagesc([-11.55 -9.55],[10.7 7.7],z_img);
colormap(gray)
plot(xunit_z, yunit_z,'red');

imagesc([-3.926 -5.926],[12.12 9.12],z1_img);
colormap(gray)
plot(xunit_z1, yunit_z1,'red');