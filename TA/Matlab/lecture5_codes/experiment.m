close all

faceW = 64; 
faceH = 64; 
numPerLine = 16; 
ShowLine = 8; 

Y = zeros(faceH*ShowLine,faceW*numPerLine); 
f=double(images);
for i=0:ShowLine-1 
  	for j=0:numPerLine-1 
    	 Y(i*faceH+1:(i+1)*faceH,j*faceW+1:(j+1)*faceW) = reshape(images(:,i*numPerLine+j+1),[faceH,faceW]); 
  	end 
end 
figure()
imagesc(Y);
colormap(gray);
axis off;
axis equal;
amr=Y(1:20,1:28)';
figure()
imagesc(amr)
%% 

x = f;

m = size(x, 2); 

G = sqrt(sum(x.^2,1)'*ones(1,m) + ones(m,1)*sum(x.^2,1) - 2*(x'*x));
 e = 0.6*median(G(:));  
 G(G>e) = 0; 
sG = sum(G, 1); 

% get rid of Inf distance for simplicity; 
i = find(sG == 0); 
idx = setdiff((1:m), i); 
G = G(idx,idx); 
m = size(G, 1); 

spy(sparse(G)); 
drawnow; 

D = graphallshortestpaths(sparse(G), 'directed', 'false');
% D(D>10^20)=0;
D2 = D.^2; 

H = eye(m) - ones(m,1)*ones(m,1)'./m; 

Dt = -0.5 * H * D2 * H; 

k = 10; 
[V, S, U] = svds(Dt, k);

dim1 = V(:,1) * sqrt(S(1,1)); 
dim2 = V(:,2) * sqrt(S(2,2)); 
%%
figure()
% imshow(Y');
%  colormap(gray);
% % % axis off;
% % % axis equal;
%  hold on
scatter(dim1, dim2,18*ones(1,698),'fill');
% hold off
%%

a=find(dim1>40 & dim1<40.2);
b=find(dim1>-46 & dim1<-45.92);
c=find(dim1>-7.2 & dim1<-7.1);
d=find(dim1>-12.1 & dim1<-11.9);
e=find(dim1>28.3 & dim1<28.34);
f=find(dim1>3.36 & dim1<3.37);
g=find(dim1>18.91 & dim1<18.93);
h=find(dim1>-27.77 & dim1<-27.75);
i=find(dim1>-28.2 & dim1<-28.1);
j=find(dim1>9.1 & dim1<9.2);
k=find(dim1>-15.2 & dim1<-15);
l=find(dim1>2.49 & dim1<2.5);
m=find(dim1>20.43 & dim1<20.45);
n=find(dim1>-19.03 & dim1<-19.01);
o=find(dim1>-2.945 & dim1<-2.94);
p=find(dim1>14.5 & dim1<14.7);
q=find(dim1>39.56 & dim1<39.58);
r=find(dim1>30.1 & dim1<30.12);
s=find(dim1>-39.56 & dim1<-39.54);

th = 0:pi/50:2*pi;
xunit_b = 1 * cos(th) + -45.98;
yunit_b = 1 * sin(th) + -3.783;

xunit_a = 1 * cos(th) + 40.1;
yunit_a = 1 * sin(th) + -21.74;
xunit_c = 1 * cos(th) + -7.17;
yunit_c = 1 * sin(th) + 10.47;

xunit_d = 1 * cos(th) + -12.01;
yunit_d = 1 * sin(th) + -26.69;

xunit_e = 1 * cos(th) + 28.32;
yunit_e = 1 * sin(th) + 12.04;


xunit_f = 1 * cos(th) + 3.361;
yunit_f = 1 * sin(th) + -6.631;
xunit_g = 1 * cos(th) + 18.92;
yunit_g = 1 * sin(th) + -20.51;
xunit_h = 1 * cos(th) + -27.76;
yunit_h = 1 * sin(th) + 4.455;
xunit_i = 1 * cos(th) + -28.14;
yunit_i = 1 * sin(th) + -15.11;
xunit_j = 1 * cos(th) + 9.127;
yunit_j = 1 * sin(th) + 8.518;
xunit_k = 1 * cos(th) + -15.1;
yunit_k = 1 * sin(th) + -7.862;
xunit_l = 1 * cos(th) + 2.498;
yunit_l = 1 * sin(th) + -24.67;
xunit_m = 1 * cos(th) + 20.44;
yunit_m = 1 * sin(th) + -4.112;

xunit_n = 1 * cos(th) + -19.02;
yunit_n = 1 * sin(th) + 14.3;
xunit_o = 1 * cos(th) + -2.943;
yunit_o = 1 * sin(th) + 21.79;
xunit_p = 1 * cos(th) + 14.6;
yunit_p = 1 * sin(th) + 18.3;
xunit_q = 1 * cos(th) + 39.57;
yunit_q = 1 * sin(th) + -7.296;
xunit_r = 1 * cos(th) + 30.11;
yunit_r = 1 * sin(th) + -17.24;
xunit_s = 1 * cos(th) + -39.55;
yunit_s = 1 * sin(th) + 5.48;
%%
close all

a_img=reshape(images(:,614),[64 64]);
b_img=reshape(images(:,572),[64 64]);
c_img=reshape(images(:,246),[64 64]);
d_img=reshape(images(:,653),[64 64]);
e_img=reshape(images(:,244),[64 64]);
f_img=reshape(images(:,340),[64 64]);
g_img=reshape(images(:,578),[64 64]);
h_img=reshape(images(:,358),[64 64]);
i_img=reshape(images(:,25),[64 64]);
j_img=reshape(images(:,291),[64 64]);
k_img=reshape(images(:,85),[64 64]);
l_img=reshape(images(:,594),[64 64]);
m_img=reshape(images(:,525),[64 64]);
n_img=reshape(images(:,182),[64 64]);
o_img=reshape(images(:,591),[64 64]);
p_img=reshape(images(:,198),[64 64]);
q_img=reshape(images(:,500),[64 64]);
r_img=reshape(images(:,93),[64 64]);
s_img=reshape(images(:,646),[64 64]);
%%
close all
figure()
% imshow(Y');
%  colormap(gray);
% % % axis off;
% % % axis equal;
%  hold on
scatter(dim1, dim2,18*ones(698,1),'fill');
hold on
imagesc([37 43],[-25 -31],a_img);
colormap(gray)
imagesc([-48 -42],[-5 -11],b_img);
colormap(gray)

imagesc([-10 -4],[8 2],c_img);
colormap(gray)
imagesc([-15 -9],[-28 -34],d_img);
colormap(gray)
imagesc([25 31],[11 5],e_img);
colormap(gray)
imagesc([0 6],[-7.7 -13.5],f_img);
colormap(gray)
imagesc([15 21],[-21.6 -27.6],g_img);
colormap(gray)
imagesc([-30.76 -24.76],[3.3 -2.7],h_img);
colormap(gray)
imagesc([-31.14 -25.14],[-16.2 -22.2],i_img);
colormap(gray)
imagesc([6.127 12.127],[7.418 1.418],j_img);
colormap(gray)
imagesc([-18.1 -12.1],[-8.96 -14.96],k_img);
colormap(gray)
imagesc([-0.51 5.49],[-25.77 -31.77],l_img);
colormap(gray)
imagesc([17.44 23.44],[-5.21 -11.21],m_img);
colormap(gray)
imagesc([-22.02 -16.02],[13.2 7.2],n_img);
colormap(gray)
imagesc([-5.94 0.06],[20.69 14.69],o_img);
colormap(gray)
imagesc([11.6 17.6],[17.2 11.2],p_img);
colormap(gray)
imagesc([36.57 42.57],[-8.39 -14.39],q_img);
colormap(gray)
imagesc([27.11 33.11],[-18.34 -24.34],r_img);
colormap(gray);
imagesc([-42.55 -37.55],[4.38 -1.62],s_img);
colormap(gray)

plot(xunit_a, yunit_a,'red');
plot(xunit_b, yunit_b,'red');
plot(xunit_c, yunit_c,'red');
plot(xunit_d, yunit_d,'red');
plot(xunit_e, yunit_e,'red');
plot(xunit_f, yunit_f,'red');
plot(xunit_g, yunit_g,'red');
plot(xunit_h, yunit_h,'red');
plot(xunit_i, yunit_i,'red');
plot(xunit_j, yunit_j,'red');
plot(xunit_k, yunit_k,'red');
plot(xunit_l, yunit_l,'red');
plot(xunit_m, yunit_m,'red');
plot(xunit_n, yunit_n,'red');
plot(xunit_o, yunit_o,'red');
plot(xunit_p, yunit_p,'red');
plot(xunit_q, yunit_q,'red');
plot(xunit_r, yunit_r,'red');
plot(xunit_s, yunit_s,'red');

hold off 