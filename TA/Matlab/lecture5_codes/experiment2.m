% lecture 5 codes

%%
f=double(ff); 

%% try out pca
x=f;
covariance = cov(x'); 
% [U, S] = eig(covariance); 
[U, S] = eigs(covariance, 3); 

% proect x to the principal direction; 
Ux = U(:,1:2)' * x; 

figure; 
scatter(Ux(1,:), Ux(2,:), 18*ones(1,m)); 

%%
% Let's try out the relation between pca and svd; 
% Pay attention to the centering in the follow trunk of codes; 
% Previously, I didn't do that intensionally, but if you review these codes, you will find 
% that to get good reconstruction error, this is necessary; 
center = mean(x, 2); 
x_center = x - center * ones(1, m); 
XTX = x_center' * x_center; 
XXT = x_center * x_center'; 
% I am going to take all 3 eigenvalues; 
k = 3; % also try k=3, and see how the reconstruction error changes
[V, S1] = eigs(XTX, k); 
[W, S2] = eigs(XXT, k); 
[Wsvd, Ssvd, Vsvd] = svds(x_center, k); 

% compare the projected x with V; 
projected_x = W(:,1:k)' * x_center; 
projected_x(:, 1:5)
% which is equal to
Ssvd * Vsvd(1:5, 1:k)' 

reconstructed_x = W(:,1:k) * projected_x + center * ones(1,m);  
reconstructed_x(:, 1:5)
x(:,1:5)
% measure the reconstruction error; 
norm(reconstructed_x - x, 'fro')
% which is equal to 
sqrt(sum(sum((reconstructed_x - x).^2)))
%%

%% build nearest neighbor graph
% compute the pairwise distance in the Euclidean space
a = sum(x.^2, 1); 
D2 = ones(m,1) * a + a' * ones(1,m) - 2*x'*x; 
D = sqrt(D2); 

% threshold the distance. only keep the nearest neighbors; 
t = 0.2 * median(D(:)); 
D(D > t) = 0; 

% visualize the sparse matrix after the thresholding; 
figure; 
spy(D); 

%% compute pairwise distance on the graph; 
tic
graphD = graphallshortestpaths(sparse(D), 'directed', false); 
toc

%%
% construct the martrix H; 
H = eye(m) - ones(m, 1) * ones(1, m) ./ m; 

ZZ = - H * (graphD.^2) * H; 


[V, D] = eigs(ZZ, 2); 
Uz = V'; 

figure; 
scatter(Uz(1,:), Uz(2,:), 18*ones(1,m), y(1,:)); 

