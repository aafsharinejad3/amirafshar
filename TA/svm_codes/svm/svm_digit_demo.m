%%
%===================================================================
% Data Preprocessing
%===================================================================

% read data
load('usps_all');

%numbers
X = [data(:,:,1)' ; data(:,:,2)'];
Y = [ones(1100,1); - ones(1100,1)];

% Create a training set
Xtrain = [X(1:1100*0.8,:) ; X(1101:1980,:)];
Ytrain = [Y(1:1100*0.8,:); Y(1101:1980,:)];

% test set
Xtest = [X(1100*0.8+1:1100,:); X(1981:2200,:)];
Ytest = [Y(1100*0.8+1:1100,:); Y(1981:2200,:)];

train_size = size(Ytrain, 1);
test_size = size(Ytest, 1);

Xtrain = double(Xtrain);
Xtest = double(Xtest);

[beta, beta0] = svm(Xtrain, Ytrain, 1e0);

Y_hat_train = sign(Xtrain * beta' + beta0);

precision1 =  sum(sum(Ytrain == 1 & Y_hat_train == 1)) / sum(sum(Ytrain == 1));
precision2 = sum(sum(Ytrain == -1 & Y_hat_train == -1)) / sum(sum(Ytrain == -1));

fprintf('train precision on class1 %g\n', precision1);
fprintf('train precision on class2 %g\n', precision2);

Y_hat_test = sign(Xtest * beta' + beta0);
precision1 =  sum(sum(Ytest == 1 & Y_hat_test == 1)) / sum(sum(Ytest == 1));
precision2 = sum(sum(Ytest == -1 & Y_hat_test == -1)) / sum(sum(Ytest == -1));
fprintf('test precision on class1 %g\n', precision1);
fprintf('test precision on class2 %g\n', precision2);


