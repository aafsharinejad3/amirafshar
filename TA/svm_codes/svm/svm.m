function [beta, beta0] = svm(X, Y, C)

N = size(X,1);
K = X * X';

G = (Y * Y') .* K;
b = - ones(N, 1);

lb = zeros(N,1);
ub = C .* ones(N,1);
opts = optimset('Algorithm', 'interior-point-convex');

%  quadratic optimizatino
alpha = quadprog(G, b, [], [], Y', 0, lb, ub, [], opts);

sv = find(alpha > 1e-10);

% Calculate the decision bound
beta = sum(bsxfun(@times, alpha(sv) .* Y(sv), X(sv,:)));
beta0 = mean(Y(sv) - sum(bsxfun(@times, alpha(sv)' .* Y(sv)', K(sv, sv)),2));

end

