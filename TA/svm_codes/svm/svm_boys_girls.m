%%
%===================================================================
% Data Preprocessing
%===================================================================

% read data
load('boys');
load('girls');

%numbers
X = [data_b' ; data_g'];
Y = [ones(116,1); - ones(63,1)];
sets=[1:23,117:129;24:46,130:142;47:69,143:155;70:92,156:168];
set5=[93:116,169:179]
% Create a training set
Xtrain = [X(24:116,:) ; X(130:179,:)];
Ytrain = [Y(24:116,:); Y(130:179,:)];

% test set
Xtest = [X(1:23,:); X(117:129,:)];
Ytest = [Y(1:23,:); Y(117:129,:)];

train_size = size(Ytrain, 1);
test_size = size(Ytest, 1);

Xtrain = double(Xtrain);
Xtest = double(Xtest);

[beta, beta0] = svm(Xtrain, Ytrain, 1000);

Y_hat_train = sign(Xtrain * beta' + beta0);

precision1 =  sum(sum(Ytrain == 1 & Y_hat_train == 1)) / sum(sum(Ytrain == 1));
precision2 = sum(sum(Ytrain == -1 & Y_hat_train == -1)) / sum(sum(Ytrain == -1));
precision = (sum(sum(Ytrain == 1 & Y_hat_train == 1))+sum(sum(Ytrain == -1 & Y_hat_train == -1)))/length(Ytrain);
fprintf('train precision on class1 %g\n', precision1);
fprintf('train precision on class2 %g\n', precision2);
fprintf('train precision on all %g\n', precision);

Y_hat_test = sign(Xtest * beta' + beta0);
precision1 =  sum(sum(Ytest == 1 & Y_hat_test == 1)) / sum(sum(Ytest == 1));
precision2 = sum(sum(Ytest == -1 & Y_hat_test == -1)) / sum(sum(Ytest == -1));
precision = (sum(sum(Ytest == 1 & Y_hat_test == 1))+sum(sum(Ytest == -1 & Y_hat_test == -1)))/length(Ytest);
fprintf('test precision on class1 %g\n', precision1);
fprintf('test precision on class2 %g\n', precision2);
fprintf('train precision on all %g\n', precision);