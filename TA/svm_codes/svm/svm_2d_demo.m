clear; close all;

C = 1e0;

load('train-data');

X = train(:,1:2);
Y = train(:, 3);

N = size(X,1);
K = X * X';

G = (Y * Y') .* K;
b = - ones(N, 1);

lb = zeros(N,1);
ub = C .* ones(N,1);
opts = optimset('Algorithm', 'interior-point-convex');

%  quadratic optimizatino
alpha = quadprog(G, b, [], [], Y', 0, lb, ub, [], opts);

sv = find(alpha > 1e-6);

class1 = X(Y == -1,:);
class2 = X(Y == 1,:);

hold on;box on;
plot(class1(:,1),class1(:,2),'.');
plot(class2(:,1),class2(:,2),'r.');

plot(X(sv,1), X(sv,2), 'ko', 'LineWidth', 2, 'MarkerSize', 10);

% Calculate the decision bound
beta = sum(bsxfun(@times, alpha(sv) .* Y(sv), X(sv,:)));
beta0 = mean(Y(sv) - sum(bsxfun(@times, alpha(sv)' .* Y(sv)', K(sv, sv)),2));

% draw the decision bound
a = -6:0.01:6;
b =  -(beta(1) * a + beta0) / beta(2);

plot(a,b, 'k-','LineWidth',2);

hold off;

Y_hat = sign(X * beta' + beta0);
precision1 = sum(sum(Y == 1 & Y_hat == 1)) / sum(sum(Y == 1));
precision2 = sum(sum(Y == -1 & Y_hat == -1)) / sum(sum(Y == -1));

fprintf('%g %g\n', precision1, precision2);