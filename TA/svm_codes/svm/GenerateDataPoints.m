%%

mu = [-2,2];


SIGMA = [1.5 0;0 1.5];
N = 200;
class1 = mvnrnd(mu, SIGMA, N);
class1 = [class1, -ones(size(class1,1),1)];

mu = [2, -2];
class2 = mvnrnd(mu, SIGMA, N);
class2 = [class2, ones(size(class2,1),1)];

hold on;box on;
plot(class1(:,1),class1(:,2),'.');
plot(class2(:,1),class2(:,2),'r.');

train = [class1;class2];

save('train-data', 'train');